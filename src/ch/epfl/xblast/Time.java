package ch.epfl.xblast;

/**
 * Interface defining some constants related to the time.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public interface Time {

    // The number of seconds per minute.
    public static final int S_PER_MIN = 60;

    // The number of milliseconds per second.
    public static final int MS_PER_S = 1000;

    // The number of microseconds per second.
    public static final int US_PER_S = 1000 * MS_PER_S;

    // The number of nanoseconds per second.
    public static final int NS_PER_S = 1000 * US_PER_S;

    // A game has a duration of 2 minutes.
    public static final int TOTAL_MINUTES = 2;
}