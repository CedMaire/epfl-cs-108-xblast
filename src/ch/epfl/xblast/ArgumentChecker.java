package ch.epfl.xblast;

/**
 * This class contains a static method to check an integer argument.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class ArgumentChecker {

    /**
     * Non instanciable class.
     */
    private ArgumentChecker() {

    }

    /**
     * Checks if the value given as parameter is bigger or equal to 0.
     * 
     * @param value
     *            the integer value we want to check
     * @return the value if it is bigger or equal to 0
     * @throws IllegalArgumentException
     *             if the parameter value is less than 0
     */
    public static int requireNonNegative(int value) {
        if (value >= 0) {

            return value;
        } else {
            throw new IllegalArgumentException();
        }
    }
}