package ch.epfl.xblast;

/**
 * Enumeration representing the different directions north, east, south and
 * west.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum Direction {

    // N = North, E = East, S = South, W = West.
    N, E, S, W;

    /**
     * Returns the opposite direction.
     * 
     * @return the opposite direction
     */
    public Direction opposite() {
        switch (this) {
        case N:
            return S;

        case E:
            return W;

        case S:
            return N;

        case W:
            return E;
        // We just throw an Error because it is supposed to never happen.
        default:
            throw new Error();
        }
    }

    /**
     * Checks if the direction is horizontal.
     * 
     * @return true if the direction is horizontal, false otherwise
     */
    public boolean isHorizontal() {

        return this == E || this == W;
    }

    /**
     * Checks if two directions are parallel.
     * 
     * @param that
     *            a direction
     * @return true if the directions are parallel, false otherwise
     */
    public boolean isParallelTo(Direction that) {

        return that.isHorizontal() == this.isHorizontal();
    }
}