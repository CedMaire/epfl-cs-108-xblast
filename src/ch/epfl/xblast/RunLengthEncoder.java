package ch.epfl.xblast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class represents a run length encoder/decoder.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class RunLengthEncoder {

    // Convention to now the byte has not been initialized.
    private static final byte NOT_INITIALIZED = -1;

    // We count 2 times a byte before encoding it.
    private static final int TIMES_BEFORE_ENCODE = 2;

    /**
     * Non instanciable class.
     */
    private RunLengthEncoder() {

    }

    /**
     * Method to run length encode a list of bytes.
     * 
     * @param listOfBytesToEncode
     *            the list of bytes
     * @return the encoded list of bytes
     * @throws IllegalArgumentException
     *             if one of the bytes is negative
     */
    public static List<Byte> encode(List<Byte> listOfBytesToEncode) {
        int counter = 0;
        byte lastElement = NOT_INITIALIZED;
        List<Byte> encodedList = new ArrayList<>();

        for (Byte currentByte : listOfBytesToEncode) {
            /*
             * If there is a negative number, then the bytes to encode are not
             * accepted.
             */
            if (currentByte < 0) {
                throw new IllegalArgumentException();
            }

            // We initialize the byte if not already done.
            if (lastElement == NOT_INITIALIZED) {
                lastElement = currentByte;
                counter++;
            }
            /*
             * As long as the value of the byte does not change we just count
             * them.
             */
            else if (currentByte == lastElement) {
                counter++;

                /*
                 * Here we manage the case if the number of repetitions are over
                 * (Byte.MAX_VALUE + TIMES_BEFORE_ENCODE).
                 */
                if (counter > Byte.MAX_VALUE + TIMES_BEFORE_ENCODE) {
                    encodedList.add((byte) (counter - TIMES_BEFORE_ENCODE));
                    encodedList.add(lastElement);

                    counter = 0;
                }
            }
            // This is the case when the value of the byte changes.
            else {
                addElement(encodedList, counter, lastElement);

                lastElement = currentByte;
                counter = 1;
            }
        }

        // We have to add the last encoding.
        addElement(encodedList, counter, lastElement);

        return Collections.unmodifiableList(encodedList);
    }

    /**
     * Method to run length decode a list of bytes.
     * 
     * @param listOfBytesToDecode
     *            the list of bytes to decode
     * @return the decoded list of bytes
     * @throws IllegalArgumentException
     *             if the last byte is negative
     */
    public static List<Byte> decode(List<Byte> listOfBytesToDecode) {
        // If the last byte is negative, the bytes to decode are not accepted.
        if (listOfBytesToDecode.get(listOfBytesToDecode.size() - 1) < 0) {
            throw new IllegalArgumentException();
        }

        List<Byte> decodedList = new ArrayList<>();
        Iterator<Byte> iterator = listOfBytesToDecode.iterator();

        // While the list of byte has a next element we continue to decode.
        while (iterator.hasNext()) {
            byte currentByte = iterator.next();

            /*
             * If the byte is negative we know it's the number of times the next
             * byte has to be added.
             */
            if (currentByte < 0) {
                byte nextByte = iterator.next();

                for (int i = 0; i < Math.abs(currentByte)
                        + TIMES_BEFORE_ENCODE; i++) {
                    decodedList.add(nextByte);
                }
            }
            /*
             * If the byte is positive, then we just add the byte to the decoded
             * list.
             */
            else {
                decodedList.add(currentByte);
            }
        }

        return Collections.unmodifiableList(decodedList);
    }

    /**
     * Private method to add the byte for the encode process.
     * 
     * @param listOfBytes
     *            the list where we want to add the byte
     * @param counter
     *            the number of time the byte has to be repeated
     * @param byteToAdd
     *            the byte to add
     */
    private static void addElement(List<Byte> listOfBytes, int counter,
            byte byteToAdd) {
        /*
         * If the counter is less or equal to TIMES_BEFORE_ENCODE.
         */
        if (counter <= TIMES_BEFORE_ENCODE) {
            for (int i = 0; i < counter; i++) {
                listOfBytes.add(byteToAdd);
            }
        }
        /*
         * If the counter is bigger than to, we put first the number of times
         * and then the byte.
         */
        else {
            listOfBytes.add((byte) -(counter - TIMES_BEFORE_ENCODE));
            listOfBytes.add(byteToAdd);
        }
    }
}