package ch.epfl.xblast.client.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import javax.swing.JComponent;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.client.GameState;
import ch.epfl.xblast.client.GameState.Player;

/**
 * This class displays/paints a state of the game XBlast.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

@SuppressWarnings("serial")
public final class XBlastComponent extends JComponent {

    // Constants for the preferred width, height and dimension.
    private static final int PREFERRED_WIDTH = 960;
    private static final int PREFERRED_HEIGHT = 688;
    private static final Dimension PREFERRED_DIMENSION = new Dimension(
            PREFERRED_WIDTH, PREFERRED_HEIGHT);

    // Constants for the preferred parameters of the font.
    private static final int PREFERRED_FONT_SIZE = 25;
    private static final String PREFERRED_FONT_NAME = new String("Arial");
    private static final Font PREFERRED_FONT = new Font(PREFERRED_FONT_NAME,
            Font.BOLD, PREFERRED_FONT_SIZE);

    // Constants for the painter of the life points.
    private static final int LIFE_POINTS_XCOORD_START = 96;
    private static final int LIFE_POINTS_XCOORD_LOW_DISTANCE = 144;
    private static final int LIFE_POINTS_XCOORD_BIG_DISTANCE = 384;
    private static final int LIFE_POINTS_YCOORD = 659;

    // The two functions to position the players correctly.
    private static final int X_MULTI = 4;
    private static final int X_ADD = -24;
    private static final int Y_MULTI = 3;
    private static final int Y_ADD = -52;
    private static final Function<Integer, Integer> X_POSITION = x -> X_MULTI
            * x + X_ADD;
    private static final Function<Integer, Integer> Y_POSITION = y -> Y_MULTI
            * y + Y_ADD;

    /*
     * Two attributes to stock the actual game state and the player id of the
     * player we are drawing for.
     */
    private GameState gameState;
    private PlayerID playerID;

    /**
     * Constructor for the XBlastComponent class that initializes the attributes
     * to null.
     */
    public XBlastComponent() {
        gameState = null;
        playerID = null;
    }

    /**
     * Getter for the preferred dimension.
     * 
     * @return the preferred dimension
     */
    @Override
    public Dimension getPreferredSize() {

        return PREFERRED_DIMENSION;
    }

    /**
     * Overrides the method paintComponent(...) in the class JComponent.
     * 
     * @param graphics0
     *            the graphical contexts
     */
    @Override
    protected void paintComponent(Graphics graphics0) {
        if (gameState != null) {
            Graphics2D graphics = (Graphics2D) graphics0;

            paintState(graphics);
            paintLifeNumbers(graphics);
            paintPlayers(graphics);
        }
    }

    /**
     * Painter for the board, the score line and the time line.
     * 
     * @param graphics
     *            the graphical contexts
     */
    private void paintState(Graphics2D graphics) {
        int xCoord = 0, yCoord = 0;

        // Here we draw the board.
        for (int i = 0; i < Cell.COUNT; i++) {
            Image cellImage = gameState.getBoardImages().get(i);
            Image cellExplosion = gameState.getBombsExplosionsImages().get(i);

            graphics.drawImage(cellImage, xCoord, yCoord, null);
            graphics.drawImage(cellExplosion, xCoord, yCoord, null);

            if (xCoord + cellImage.getWidth(null) < getPreferredSize()
                    .getWidth()) {
                xCoord += cellImage.getWidth(null);
            } else {
                xCoord = 0;
                yCoord += cellImage.getHeight(null);
            }
        }

        // Then we draw the score line.
        for (Image image : gameState.getScoreLineImages()) {
            graphics.drawImage(image, xCoord, yCoord, null);

            xCoord += image.getWidth(null);
        }

        xCoord = 0;
        yCoord += gameState.getScoreLineImages().get(0).getHeight(null);

        // And now we draw the time line.
        for (Image image : gameState.getTimeLineImages()) {
            graphics.drawImage(image, xCoord, yCoord, null);

            xCoord += image.getWidth(null);
        }
    }

    /**
     * Painter for the life points of the players.
     * 
     * @param graphics
     *            the graphical contexts
     */
    private void paintLifeNumbers(Graphics2D graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(PREFERRED_FONT);

        int xCoord = LIFE_POINTS_XCOORD_START;
        for (Player player : gameState.getPlayersList()) {
            graphics.drawString(String.valueOf(player.getLives()), xCoord,
                    LIFE_POINTS_YCOORD);

            xCoord += LIFE_POINTS_XCOORD_LOW_DISTANCE;
            if (player.getID() == PlayerID.PLAYER_2) {
                xCoord += LIFE_POINTS_XCOORD_BIG_DISTANCE;
            }
        }
    }

    /**
     * Painter for the players.
     * 
     * @param graphics
     *            the graphical contexts
     */
    private void paintPlayers(Graphics2D graphics) {
        for (Player player : orderOfPlayers()) {
            graphics.drawImage(player.getImage(),
                    X_POSITION.apply(player.getPosition().x()),
                    Y_POSITION.apply(player.getPosition().y()), null);
        }
    }

    /**
     * Method to get the list of the players in the right order for the painting
     * process.
     * 
     * @return the list of the players in the right order
     */
    private List<Player> orderOfPlayers() {
        Comparator<Player> comparatorYCoord = (p0, p1) -> Integer
                .compare(p0.getPosition().y(), p1.getPosition().y());
        Comparator<Player> comparatorPlayerID = new Comparator<Player>() {
            @Override
            public int compare(Player p0, Player p1) {
                List<PlayerID> idRotation = IDRotation();

                int p0Index = idRotation.indexOf(p0.getID());
                int p1Index = idRotation.indexOf(p1.getID());

                return Integer.compare(p0Index, p1Index);
            }
        };

        List<Player> listOfPlayers = new ArrayList<>(
                gameState.getPlayersList());
        Collections.sort(listOfPlayers,
                comparatorYCoord.thenComparing(comparatorPlayerID));

        return listOfPlayers;
    }

    /**
     * Method to get a list of PlayerIDs in the right rotation.
     * 
     * @return the list of PlayerIDs in the right rotation
     */
    private List<PlayerID> IDRotation() {
        List<PlayerID> standardRotation = new ArrayList<>(
                Arrays.asList(PlayerID.values()));

        while (standardRotation.get(standardRotation.size() - 1) != playerID) {
            PlayerID temp = standardRotation
                    .remove(standardRotation.size() - 1);
            standardRotation.add(0, temp);
        }

        return standardRotation;
    }

    /**
     * Setter for the gameState and playerID attributes. This method also calls
     * the method repaint() to update the graphics.
     * 
     * @param gameState
     *            the new game state to set
     * @param playerID
     *            the new player id to set
     */
    public void setGameState(GameState gameState, PlayerID playerID) {
        this.gameState = gameState;
        this.playerID = playerID;

        repaint();
    }
}