package ch.epfl.xblast.client.graphics;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import ch.epfl.xblast.PlayerAction;

/**
 * This class represents a keyboard event listener.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class KeyboardEventHandler extends KeyAdapter {

    /*
     * The KeyboardEventHandler has a map that maps a PlayerAction to the
     * integer of the KeyEvent and a consumer for the PlayerActions.
     */
    private final Map<Integer, PlayerAction> actionToKey;
    private final Consumer<PlayerAction> playerActionConsumer;

    /**
     * Constructor for the KeyboardEventHandler class that initializes the
     * attributes.
     * 
     * @param actionToKey
     *            the map that maps the PlayerActions to the integer of the
     *            KeyEvents
     * @param playerActionConsumer
     *            the consumer for the PlayerActions
     */
    public KeyboardEventHandler(Map<Integer, PlayerAction> actionToKey,
            Consumer<PlayerAction> playerActionConsumer) {
        this.actionToKey = Collections
                .unmodifiableMap(Objects.requireNonNull(actionToKey));
        this.playerActionConsumer = Objects
                .requireNonNull(playerActionConsumer);
    }

    /**
     * Overriding of the method keyPressed(...) in KeyAdapter. This method is
     * invoked when a key has been pressed.
     * 
     * @param keyEvent
     *            the key event
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (actionToKey.containsKey(keyEvent.getKeyCode())) {
            playerActionConsumer.accept(actionToKey.get(keyEvent.getKeyCode()));
        }
    }
}