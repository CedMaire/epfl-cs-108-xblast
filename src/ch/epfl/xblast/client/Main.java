package ch.epfl.xblast.client;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.net.InetSocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import ch.epfl.xblast.PlayerAction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.Time;
import ch.epfl.xblast.client.graphics.KeyboardEventHandler;
import ch.epfl.xblast.client.graphics.XBlastComponent;

/**
 * This class is the main class of the client for the game XBlast2016.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Main {

    // The name of the window.
    private static final String WINDOW_NAME = "XBlast 2016";

    /*
     * Name of the default host where the server is located if none is specified
     * and the default port to use.
     */
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2016;

    // We try to connect to the server 5 times per second.
    private static final int TIMES_PER_SECOND = 5;
    private static final int CONNECT_SLEEP = Time.MS_PER_S / TIMES_PER_SECOND;

    /*
     * Each PlayerAction is coded in only one byte. And the player ID we receive
     * is also encoded in only one byte.
     */
    private static final int PLAYER_ACTION_BYTE = 1;
    private static final int PLAYER_ID_BYTE = 1;

    // The maximal size of the serial and the size for the buffer.
    private static final int MAX_SERIAL_SIZE = 409;
    private static final int BUFFER_RECEIVER_SIZE = MAX_SERIAL_SIZE
            + PLAYER_ID_BYTE;

    // The two buffer used to send and receive the informations.
    private static final ByteBuffer bufferSender = ByteBuffer
            .allocate(PLAYER_ACTION_BYTE);
    private static final ByteBuffer bufferReceiver = ByteBuffer
            .allocate(BUFFER_RECEIVER_SIZE);

    // The XBlastComponent used for the GUI.
    private static XBlastComponent xBlastComponent;

    /*
     * Three variables to stock the serial of the game state, the ID of the
     * player and the host address.
     */
    private static final List<Byte> serial = new ArrayList<>();
    private static PlayerID playerID;
    private static InetSocketAddress hostAddress = new InetSocketAddress(
            DEFAULT_HOST, DEFAULT_PORT);

    /**
     * Main method that is executed on the client.
     * 
     * @param args
     *            optionally contains the IP address of the server
     */
    public static void main(String[] args) throws Exception {
        // We initialize the host address or leave the default one.
        if (args.length > 0) {
            hostAddress = new InetSocketAddress(args[0], DEFAULT_PORT);
        }

        try (DatagramChannel channel = DatagramChannel
                .open(StandardProtocolFamily.INET)) {
            SwingUtilities.invokeAndWait(() -> createUI(channel));

            /*
             * We want to try to connect to the server and we don't want to
             * wait.
             */
            channel.configureBlocking(false);
            do {
                bufferSender.clear();
                bufferSender.put((byte) PlayerAction.JOIN_GAME.ordinal());
                bufferSender.rewind();

                channel.send(bufferSender, hostAddress);

                Thread.sleep(CONNECT_SLEEP);
            } while (channel.receive(bufferReceiver) == null);

            /*
             * We just received our first game state, it is in the buffer. From
             * now on we wait until we actually receive the next game state.
             */
            channel.configureBlocking(true);
            while (true) {
                serial.clear();
                bufferReceiver.flip();

                playerID = PlayerID.values()[bufferReceiver.get()];

                while (bufferReceiver.hasRemaining()) {
                    serial.add(bufferReceiver.get());
                }

                xBlastComponent.setGameState(
                        GameStateDeserializer.deserialize(serial), playerID);

                // We can now receive the next game state from the server.
                bufferReceiver.clear();
                channel.receive(bufferReceiver);
            }
        }
    }

    /**
     * Event dispatch thread for the GUI.
     * 
     * @param channel
     *            the communication channel used
     */
    private static void createUI(DatagramChannel channel) {
        xBlastComponent = new XBlastComponent();
        JFrame window = new JFrame(WINDOW_NAME);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.add(xBlastComponent, BorderLayout.CENTER);
        window.pack();
        window.setResizable(false);
        window.setVisible(true);

        xBlastComponent.addKeyListener(new KeyboardEventHandler(
                getActionToKeyMap(), getSendActionConsumer(channel)));

        xBlastComponent.requestFocusInWindow();
    }

    /**
     * Method to create the map containing the different actions of the player.
     * 
     * @return a map containing the action of the player
     */
    private static Map<Integer, PlayerAction> getActionToKeyMap() {
        Map<Integer, PlayerAction> temporaryMap = new HashMap<>();

        temporaryMap.put(KeyEvent.VK_UP, PlayerAction.MOVE_N);
        temporaryMap.put(KeyEvent.VK_RIGHT, PlayerAction.MOVE_E);
        temporaryMap.put(KeyEvent.VK_DOWN, PlayerAction.MOVE_S);
        temporaryMap.put(KeyEvent.VK_LEFT, PlayerAction.MOVE_W);
        temporaryMap.put(KeyEvent.VK_SPACE, PlayerAction.DROP_BOMB);
        temporaryMap.put(KeyEvent.VK_SHIFT, PlayerAction.STOP);

        return Collections.unmodifiableMap(temporaryMap);
    }

    /**
     * Method to create the consumer that will be used to send the actions to
     * the server.
     * 
     * @param channel
     *            the communication channel used
     * @return the consumer that sends the actions to the server
     */
    private static Consumer<PlayerAction> getSendActionConsumer(
            DatagramChannel channel) {

        return action -> {
            bufferSender.clear();
            bufferSender.put((byte) action.ordinal());
            bufferSender.rewind();

            try {
                channel.send(bufferSender, hostAddress);
            } catch (Exception e) {
                // We hope this will not happen. :)
            }
        };
    }
}