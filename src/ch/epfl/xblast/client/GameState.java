package ch.epfl.xblast.client;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;

/**
 * This class represents the state of a game (client-side).
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class GameState {

    /*
     * Client-side the game state is composed of 5 attributes: the images of the
     * board, the images of the bombs and explosions, the list of the players,
     * the images of the score line and the images of the time line.
     */
    private final List<Image> boardImages;
    private final List<Image> bombsExplosionsImages;
    private final List<Player> playersList;
    private final List<Image> scoreLineImages;
    private final List<Image> timeLineImages;

    /**
     * Constructor for the client-side game state. It initializes all the
     * attributes.
     * 
     * @param boardImages
     *            the list of images for the board
     * @param bombExplosionImages
     *            the list of images for the bombs and explosions
     * @param playerList
     *            the list of players
     * @param scoreLineImages
     *            the list of images for the score line
     * @param timeLineImages
     *            the list of images for the time line
     * @throws NullPointerException
     *             if one of the parameters is null
     */
    public GameState(List<Image> boardImages, List<Image> bombsExplosionsImages,
            List<Player> playersList, List<Image> scoreLineImages,
            List<Image> timeLineImages) {
        this.boardImages = Collections.unmodifiableList(
                new ArrayList<Image>(Objects.requireNonNull(boardImages)));
        this.bombsExplosionsImages = Collections
                .unmodifiableList(new ArrayList<Image>(
                        Objects.requireNonNull(bombsExplosionsImages)));
        this.playersList = Collections.unmodifiableList(
                new ArrayList<Player>(Objects.requireNonNull(playersList)));
        this.scoreLineImages = Collections.unmodifiableList(
                new ArrayList<Image>(Objects.requireNonNull(scoreLineImages)));
        this.timeLineImages = Collections.unmodifiableList(
                new ArrayList<Image>(Objects.requireNonNull(timeLineImages)));
    }

    /**
     * Getter for the attribute boardImages.
     * 
     * @return the list of images to use for the board (row major order)
     */
    public List<Image> getBoardImages() {

        return boardImages;
    }

    /**
     * Getter for the attribute bombsExplosionsImages.
     * 
     * @return the list of images to use for the bombs and explosions (row major
     *         order)
     */
    public List<Image> getBombsExplosionsImages() {

        return bombsExplosionsImages;
    }

    /**
     * Getter for the attribute playersList.
     * 
     * @return the list of players
     */
    public List<Player> getPlayersList() {

        return playersList;
    }

    /**
     * Getter for the attribute scoreLineImages.
     * 
     * @return the list of images to use for the score line
     */
    public List<Image> getScoreLineImages() {

        return scoreLineImages;
    }

    /**
     * Getter for the attribute timeLineImages.
     * 
     * @return the list of images to use for the time line
     */
    public List<Image> getTimeLineImages() {

        return timeLineImages;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------------------------------------

    public static final class Player {

        /**
         * This class emulates a player (client-side).
         * 
         * @author Maire Cedric (259314)
         * @author Délèze Benjamin (259992)
         *
         */

        /*
         * Client-side a player is composed of its ID, its lives, its position
         * and its image.
         */
        private final PlayerID id;
        private final int lives;
        private final SubCell position;
        private final Image image;

        /**
         * Constructor of the class GameState.Player that initializes all the
         * attributes.
         * 
         * @param id
         *            the ID of the player
         * @param lives
         *            the number of lives of the player
         * @param position
         *            the position of the player
         * @param image
         *            the image of the player
         * @throws IllegalArgumentException
         *             if the parameter lives is less than 0
         * @throws NullPointerException
         *             if the parameter position of image is null
         */
        public Player(PlayerID id, int lives, SubCell position, Image image) {
            this.id = Objects.requireNonNull(id);
            this.lives = ArgumentChecker.requireNonNegative(lives);
            this.position = Objects.requireNonNull(position);
            this.image = image;
        }

        /**
         * Getter for the attribute id.
         * 
         * @return the id of the player
         */
        public PlayerID getID() {

            return id;
        }

        /**
         * Getter for the attribute lives.
         * 
         * @return the number of lives of the player
         */
        public int getLives() {

            return lives;
        }

        /**
         * Getter for the attribute position.
         * 
         * @return the position of the player
         */
        public SubCell getPosition() {

            return position;
        }

        /**
         * Getter for the attribute image.
         * 
         * @return the image of the player
         */
        public Image getImage() {

            return image;
        }
    }
}