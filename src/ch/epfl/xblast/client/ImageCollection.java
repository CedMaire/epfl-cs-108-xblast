package ch.epfl.xblast.client;

import java.awt.Image;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.imageio.ImageIO;

/**
 * This class represents an image collection coming from a directory and indexed
 * by an integer.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class ImageCollection {

    // Integer that states on how many digits the index of an image is coded.
    private static final int DIGIT_LENGTH = 3;

    // Map that maps the index of an image to its file, the actual image.
    private final Map<Byte, Image> indexMapToImage;

    /**
     * Constructor of the class ImageCollection. It initializes the map
     * indexMapToImage.
     * 
     * @param directoryName
     *            the name of the directory where the images are
     */
    public ImageCollection(String dirName) {
        Map<Byte, Image> temporaryMap = new HashMap<>();

        try {
            File dir = new File(ImageCollection.class.getClassLoader()
                    .getResource(dirName).toURI());

            for (File currentImage : dir.listFiles()) {
                try {
                    byte index = (byte) Integer.parseInt(
                            currentImage.getName().substring(0, DIGIT_LENGTH));
                    Image image = ImageIO.read(currentImage);

                    temporaryMap.put(index, image);
                } catch (Exception e) {
                    // We hope this will not happen. :)
                }
            }
        } catch (Exception e) {
            // We hope this will not happen. :)
        }

        indexMapToImage = Collections.unmodifiableMap(temporaryMap);
    }

    /**
     * Method to get the image corresponding to the index given as parameter.
     * 
     * @param index
     *            index we want the image of
     * @return the image corresponding to the index
     * @throws NoSuchElementException
     *             if the index is invalid
     */
    public Image image(byte index) {
        if (imageOrNull(index) == null) {
            throw new NoSuchElementException();
        } else {

            return indexMapToImage.get(index);
        }
    }

    /**
     * Method to get the image corresponding to the index given as parameter.
     * 
     * @param index
     *            index we want the image of
     * @return the image corresponding to the index or null if the index is
     *         invalid
     */
    public Image imageOrNull(byte index) {

        return indexMapToImage.get(index);
    }
}