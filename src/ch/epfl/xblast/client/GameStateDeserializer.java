package ch.epfl.xblast.client;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.RunLengthEncoder;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.client.GameState.Player;

/**
 * This class represents a game state deserializer.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class GameStateDeserializer {

    // These are the different image collections needed.
    private static final ImageCollection BLOCK_IMAGE_COLLECTION = new ImageCollection(
            "block");
    private static final ImageCollection EXPLOSION_IMAGE_COLLECTION = new ImageCollection(
            "explosion");
    private static final ImageCollection PLAYER_IMAGE_COLLECTION = new ImageCollection(
            "player");
    private static final ImageCollection SCORE_IMAGE_COLLECTION = new ImageCollection(
            "score");

    /*
     * This is the length of the serial for the players. And the length of the
     * serial of each player individually. The number 4 is the number of
     * attributes of the class GameState.Player.
     */
    private static final int PLAYERS_LIST_LENGTH = 4 * PlayerID.values().length;
    private static final int PLAYER_LIST_LENGTH = PLAYERS_LIST_LENGTH
            / PlayerID.values().length;

    // Some fixed image indexes.
    private static final byte TEXT_MIDDLE = 10;
    private static final byte TEXT_RIGHT = 11;
    private static final byte TILE_VOID = 12;
    private static final byte LED_OFF_BYTE = 20;
    private static final byte LED_ON_BYTE = 21;

    /*
     * There are 60 images to represent the time line. We also need 8 tiles to
     * fill the space in the middle of the score line.
     */
    private static final int TIME_CODE = 60;
    private static final int SCORE_FILLING = 8;

    /**
     * Non instanciable class.
     */
    private GameStateDeserializer() {

    }

    /**
     * This method deserializes the serial received as parameter and return a
     * representation of the client-side game state.
     * 
     * @param serial
     *            the serial representing the game state
     * @return a client-side game state
     */
    public static GameState deserialize(List<Byte> serial) {
        final int BOARD_LIST_START = 1;
        final int BOARD_LIST_END = serial.get(0) + 1;
        final int BOMBS_EXPLOSIONS_LIST_START = BOARD_LIST_END + 1;
        final int BOMBS_EXPLOSIONS_LIST_END = BOMBS_EXPLOSIONS_LIST_START
                + serial.get(BOARD_LIST_END);
        final int PLAYERS_LIST_START = BOMBS_EXPLOSIONS_LIST_END;
        final int PLAYERS_LIST_END = PLAYERS_LIST_START + PLAYERS_LIST_LENGTH;

        List<Byte> gameBoardSerial = new ArrayList<>(
                serial.subList(BOARD_LIST_START, BOARD_LIST_END));
        List<Byte> bombExplosionSerial = new ArrayList<>(serial.subList(
                BOMBS_EXPLOSIONS_LIST_START, BOMBS_EXPLOSIONS_LIST_END));
        List<Byte> playersSerial = new ArrayList<>(
                serial.subList(PLAYERS_LIST_START, PLAYERS_LIST_END));
        byte remainingTime = serial.get(serial.size() - 1);

        return new GameState(gameBoardDeserializer(gameBoardSerial),
                bombExplosionDeserializer(bombExplosionSerial),
                playersDeserializer(playersSerial),
                scoreLineDeserializer(playersSerial),
                timeLineDeserializer(remainingTime));
    }

    /**
     * Method to deserialize the game board serial.
     * 
     * @param gameBoardSerial
     *            the serial of the game board
     * @return the list of images corresponding to the game state
     */
    private static List<Image> gameBoardDeserializer(
            List<Byte> gameBoardSerial) {
        Image[] rowMajorOrderBoard = new Image[Cell.COUNT];
        List<Byte> decodedGameBoardSerial = RunLengthEncoder
                .decode(gameBoardSerial);

        int counter = 0;
        for (Cell currentCell : Cell.SPIRAL_ORDER) {
            rowMajorOrderBoard[currentCell
                    .rowMajorIndex()] = BLOCK_IMAGE_COLLECTION
                            .image(decodedGameBoardSerial.get(counter));
            counter++;
        }

        return Arrays.asList(rowMajorOrderBoard);
    }

    /**
     * Method to deserialize the bombs and explosions serial.
     * 
     * @param bombExplosionSerial
     *            the bombs and explosions serial
     * @return the list of images corresponding to the bombs and explosions in
     *         the game
     */
    private static List<Image> bombExplosionDeserializer(
            List<Byte> bombExplosionSerial) {
        List<Image> temporaryImageList = new ArrayList<>();
        List<Byte> decodedBombExplosionSerial = RunLengthEncoder
                .decode(bombExplosionSerial);

        for (Byte currentByte : decodedBombExplosionSerial) {
            temporaryImageList
                    .add(EXPLOSION_IMAGE_COLLECTION.imageOrNull(currentByte));
        }

        return temporaryImageList;
    }

    /**
     * Method to deserialize the players serial.
     * 
     * @param playersSerial
     *            the serial of the players
     * @return a list of client-side players
     */
    private static List<Player> playersDeserializer(List<Byte> playersSerial) {
        List<Player> temporaryPlayerList = new ArrayList<>();

        for (int i = 0; i < PLAYERS_LIST_LENGTH; i = i + PLAYER_LIST_LENGTH) {
            temporaryPlayerList.add(new Player(PlayerID.values()[i / 4],
                    playersSerial.get(i),
                    new SubCell(Byte.toUnsignedInt(playersSerial.get(i + 1)),
                            Byte.toUnsignedInt(playersSerial.get(i + 2))),
                    PLAYER_IMAGE_COLLECTION
                            .imageOrNull(playersSerial.get(i + 3))));
        }

        return temporaryPlayerList;
    }

    /**
     * Method to deserialize the score line.
     * 
     * @param playersSerial
     *            the serial of the players
     * @return a list of images for the score line
     */
    private static List<Image> scoreLineDeserializer(List<Byte> playersSerial) {
        List<Image> temporaryImageList = new ArrayList<>();

        for (int i = 0; i < PLAYERS_LIST_LENGTH; i += PLAYER_LIST_LENGTH) {
            if (playersSerial.get(i) <= 0) {
                temporaryImageList.add(
                        SCORE_IMAGE_COLLECTION.image((byte) ((i / 2) + 1)));
            } else {
                temporaryImageList
                        .add(SCORE_IMAGE_COLLECTION.image((byte) (i / 2)));
            }

            temporaryImageList.add(SCORE_IMAGE_COLLECTION.image(TEXT_MIDDLE));
            temporaryImageList.add(SCORE_IMAGE_COLLECTION.image(TEXT_RIGHT));

            if (i == PLAYER_LIST_LENGTH) {
                temporaryImageList.addAll(Collections.nCopies(SCORE_FILLING,
                        SCORE_IMAGE_COLLECTION.image(TILE_VOID)));
            }
        }

        return temporaryImageList;
    }

    /**
     * Method to deserialize the time line.
     * 
     * @param remainingTime
     *            the remaining time of the game
     * @return a list of images for the time line
     */
    private static List<Image> timeLineDeserializer(byte remainingTime) {
        List<Image> temporaryImageList = new ArrayList<>();

        temporaryImageList.addAll(Collections.nCopies(remainingTime,
                SCORE_IMAGE_COLLECTION.image(LED_ON_BYTE)));
        temporaryImageList.addAll(Collections.nCopies(TIME_CODE - remainingTime,
                SCORE_IMAGE_COLLECTION.image(LED_OFF_BYTE)));

        return temporaryImageList;
    }
}