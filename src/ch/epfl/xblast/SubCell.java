package ch.epfl.xblast;

/**
 * This class emulates a Sub-cell; a Cell is formed of 16x16 Sub-cells.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class SubCell {

    /*
     * A constant to stock the number of sub-cells needed in column and row to
     * form one cell.
     */
    public static final int SUBDIVISION = 16;

    /*
     * Three static constants to stock the number of columns, rows and total
     * sub-cells.
     */
    public static final int COLUMNS = SUBDIVISION * Cell.COLUMNS;
    public static final int ROWS = SUBDIVISION * Cell.ROWS;
    public static final int COUNT = COLUMNS * ROWS;

    // Two constants to stock the x and y coordinates of each cell.
    private final int coordX;
    private final int coordY;

    /**
     * The constructor of the SubCell class, it initializes the x and y
     * coordinates.
     * 
     * @param x
     *            the x coordinate
     * @param y
     *            the y coordinate
     */
    public SubCell(int x, int y) {
        coordX = Math.floorMod(x, COLUMNS);
        coordY = Math.floorMod(y, ROWS);
    }

    /**
     * Getter for the x coordinate.
     * 
     * @return the x coordinate
     */
    public int x() {

        return coordX;
    }

    /**
     * Getter for the y coordinate.
     * 
     * @return the y coordinate
     */
    public int y() {

        return coordY;
    }

    /**
     * Finds the central sub-cell of the cell given in parameter.
     * 
     * @param cell
     *            the cell we want the central sub-cell of
     * @return the central sub-cell
     */
    public static SubCell centralSubCellOf(Cell cell) {

        return new SubCell((cell.x() * SUBDIVISION) + (SUBDIVISION / 2),
                (cell.y() * SUBDIVISION) + (SUBDIVISION / 2));
    }

    /**
     * Computes the Manhattan distance to the nearest central sub-cell.
     * 
     * @return the Manhattan distance
     */
    public int distanceToCentral() {
        SubCell nearestCentralSubCell = centralSubCellOf(containingCell());

        return Math.abs(this.x() - nearestCentralSubCell.x())
                + Math.abs(this.y() - nearestCentralSubCell.y());
    }

    /**
     * Method that checks if the sub-cell is a central sub-cell of a cell.
     * 
     * @return true if it is a central sub-cell, false otherwise
     */
    public boolean isCentral() {

        return distanceToCentral() == 0;
    }

    /**
     * Method to get the neighbor of the sub-cell in the desired direction.
     * 
     * @param d
     *            the desired direction
     * @return the neighbor sub-cell
     */
    public SubCell neighbor(Direction d) {
        switch (d) {
        case N:
            return new SubCell(x(), y() - 1);

        case E:
            return new SubCell(x() + 1, y());

        case S:
            return new SubCell(x(), y() + 1);

        case W:
            return new SubCell(x() - 1, y());
        // We just throw an Error because it is supposed to never happen.
        default:
            throw new Error();
        }
    }

    /**
     * Returns the cell containing the sub-cell.
     * 
     * @return the cell containing the sub-cell
     */
    public Cell containingCell() {

        return new Cell((int) (x() / SUBDIVISION), (int) (y() / SUBDIVISION));
    }

    @Override
    /**
     * Redefinition of the equals() method of the Object class.
     * 
     * @param that
     *            the sub-cell to be compared with
     * @return true if they have the same class, x and y coordinate; false
     *         otherwise
     */
    public boolean equals(Object that) {

        return that.getClass() == this.getClass()
                && ((SubCell) that).x() == this.x()
                && ((SubCell) that).y() == this.y();
    }

    @Override
    /**
     * Redefinition of the toString() method of the Object class.
     * 
     * @return a string showing the x and y coordinate of the sub-cell (Format:
     *         (xCoordinate, yCoordinate))
     */
    public String toString() {

        return "(" + x() + ", " + y() + ")";
    }

    @Override
    /**
     * Redefinition of the hashCode() method of the Object class.
     * 
     * @return a hash code value for this object
     */
    public int hashCode() {

        return y() * COLUMNS + x();
    }
}