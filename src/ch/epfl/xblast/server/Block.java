package ch.epfl.xblast.server;

import java.util.NoSuchElementException;

/**
 * This enumeration defines the different types of blocks.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum Block {

    /*
     * FREE = free block for bonuses, bombs and players; INDESTRUCTIBLE_WALL = a
     * wall that can't be destroyed; DESTRUCTIBLE_WALL = a wall that can be
     * destroyed; CRUMBLING_WALL = a wall that is collapsing; BONUS_BOMB = block
     * for the bomb bonus (with the associated INC_BOMB bonus); BONUS_RANGE =
     * block for the range bonus (with the associated INC_RANGE bonus)
     */
    FREE, INDESTRUCTIBLE_WALL, DESTRUCTIBLE_WALL, CRUMBLING_WALL, BONUS_BOMB(
            Bonus.INC_BOMB), BONUS_RANGE(Bonus.INC_RANGE);

    // Variable stocking the maybe associated bonus.
    private final Bonus maybeAssociatedBonus;

    /**
     * Constructor for the blocks having an associated bonus.
     * 
     * @param maybeAssociatedBonus
     *            the associated bonus
     */
    private Block(Bonus maybeAssociatedBonus) {
        this.maybeAssociatedBonus = maybeAssociatedBonus;
    }

    /**
     * Constructor for the block that does not have an associated bonus.
     */
    private Block() {
        maybeAssociatedBonus = null;
    }

    /**
     * Method to know if the block is free.
     * 
     * @return true if the block is FREE, false otherwise
     */
    public boolean isFree() {

        return this == FREE;
    }

    /**
     * Method to know if the block can host a player.
     * 
     * @return true if the block is free or hosts a bonus, false otherwise
     */
    public boolean canHostPlayer() {

        return isFree() || isBonus();
    }

    /**
     * Method to know if the block casts a shadow on the board.
     * 
     * @return true if the block is a wall (ends with _WALL), false otherwise
     */
    public boolean castsShadow() {

        return this == INDESTRUCTIBLE_WALL || this == DESTRUCTIBLE_WALL
                || this == CRUMBLING_WALL;
    }

    /**
     * Method to know if the block is a bonus.
     * 
     * @return true if the block is either BONUS_BOMB or BONUS_RANGE, false
     *         otherwise
     */
    public boolean isBonus() {

        return this == BONUS_BOMB || this == BONUS_RANGE;
    }

    /**
     * Method to get the maybe associated bonus of the block.
     * 
     * @return the associated bonus (if there is one)
     * @throws NoSuchElementException
     *             if there is no associated bonus
     */
    public Bonus associatedBonus() {
        if (this.maybeAssociatedBonus == null) {
            throw new NoSuchElementException();
        } else {

            return this.maybeAssociatedBonus;
        }
    }
}