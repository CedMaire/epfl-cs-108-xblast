package ch.epfl.xblast.server;

import java.util.Objects;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.server.Player.LifeState.State;

/**
 * This class emulates a player (server-side).
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Player {

    /*
     * Constants that qualify a player (its ID, its life sate (invulnerable...),
     * its pair of direction and position, the maximum of simultaneous bombs he
     * can place and the range his bombs will explode in).
     */
    private final PlayerID id;
    private final Sq<LifeState> lifeStates;
    private final Sq<DirectedPosition> directedPos;
    private final int maxBombs;
    private final int bombRange;

    /**
     * Constructor of the Player class initializing all the attributes with the
     * given parameters.
     * 
     * @param id
     *            the id of the player
     * @param lifeStates
     *            a sequence representing the life states of the player
     * @param directedPos
     *            the pair of direction and position to initialize the player
     *            with
     * @param maxBombs
     *            the maximum of bombs the player can place simultaneous
     * @param bombRange
     *            the range the bombs of the player will explode in
     * @throws NullPointerException
     *             if one of the parameters id, lifStates or directedPos is null
     * @throws IllegalArgumentException
     *             if one of the parameters maxBombs or bombRange is strictly
     *             negative
     */
    public Player(PlayerID id, Sq<LifeState> lifeStates,
            Sq<DirectedPosition> directedPos, int maxBombs, int bombRange) {
        this.id = Objects.requireNonNull(id);

        if (Objects.requireNonNull(lifeStates).head().lives() == 0) {
            this.lifeStates = Sq.constant(new LifeState(0, State.DEAD));
        } else {
            this.lifeStates = lifeStates;
        }

        this.directedPos = Objects.requireNonNull(directedPos);
        this.maxBombs = ArgumentChecker.requireNonNegative(maxBombs);
        this.bombRange = ArgumentChecker.requireNonNegative(bombRange);
    }

    /**
     * Constructor of the Player class initializing all the attributes with the
     * given parameters. -The sequence of states is such that the player is
     * INVULNERABLE during PLAYER_INVULNERABLE_TICKS ticks and then VULNERABLE.
     * -The sequence of directed position represents the player stopped on the
     * position given by the parameter and looking to the south.
     * 
     * @param id
     *            the id of the player
     * @param lives
     *            the number of lives the player will have
     * @param position
     *            the cell the player will stand on (on the cell's central
     *            sub-cell)
     * @param maxBombs
     *            the maximum of bombs the player can place simultaneous
     * @param bombRange
     *            the range the bombs of the player will explode in
     * @throws NullPointerException
     *             if one of the parameters id or position is null
     * @throws IllegalArgumentException
     *             if one of the parameters lives, maxBombs or bombRange is
     *             strictly negative
     */
    public Player(PlayerID id, int lives, Cell position, int maxBombs,
            int bombRange) {

        this(id, Sq
                .repeat(Ticks.PLAYER_INVULNERABLE_TICKS,
                        new LifeState(lives, State.INVULNERABLE))
                .concat(Sq.constant(new LifeState(lives, State.VULNERABLE))),
                DirectedPosition.stopped(new DirectedPosition(
                        SubCell.centralSubCellOf(
                                Objects.requireNonNull(position)),
                        Direction.S)),
                maxBombs, bombRange);
    }

    /**
     * Getter for the identity of the player.
     * 
     * @return the id attribute
     */
    public PlayerID id() {

        return id;
    }

    /**
     * Getter for the sequence of life states.
     * 
     * @return the lifeStates attribute
     */
    public Sq<LifeState> lifeStates() {

        return lifeStates;
    }

    /**
     * Getter for the current pair of life and state of the player.
     * 
     * @return the head of the lifeStates sequence attribute
     */
    public LifeState lifeState() {

        return lifeStates().head();
    }

    /**
     * Creates the sequence of life states for the next life of the player. -The
     * sequence always starts with PLAYER_INVULNERABLE_TICKS times the life
     * state composed of its number of lives and the states DYING. -If its
     * number of lives is less or equal to 1, then an infinite sequence of dead
     * life state (number of lives = 0 and state is DEAD) is added. -If its
     * number of lives is bigger than 1, then an infinite sequence is added. The
     * sequence is composed of PLAYER_INVULNERABLE_TICKS times a life state
     * composed of the number of lives minus one and the state INVULNERABLE and
     * then an infinite sequence composed of life states composed of the number
     * of lives minus one and the state VULNERABLE.
     * 
     * @return the sequence of life states for the next life of the player
     */
    public Sq<LifeState> statesForNextLife() {

        return Sq
                .repeat(Ticks.PLAYER_DYING_TICKS,
                        new LifeState(lives(), State.DYING))
                .concat(completeSqOfStatesForNextLife(lives()));
    }

    /**
     * Given a number of lives the method returns a sequence of life states. -If
     * its number of lives is less or equal to 1, then an infinite sequence of
     * dead life state (number of lives = 0 and sate is DEAD) is returned. -If
     * its number of lives is bigger than 1, then an infinite sequence is
     * returned. The sequence is composed of PLAYER_INVULNERABLE_TICKS times a
     * life state composed of the number of lives minus one and the state
     * INVULNERABLE; and then an infinite sequence composed of life states
     * composed of the number of lives minus one and the state VULNERABLE.
     * 
     * @param numberOfLives
     *            the number of current lives of the player
     * @return the sequence composed of life states according to the conditions
     */
    private static Sq<LifeState> completeSqOfStatesForNextLife(
            int numberOfLives) {
        if (numberOfLives <= 1) {

            return Sq.constant(new LifeState(0, State.DEAD));
        } else {

            return Sq
                    .repeat(Ticks.PLAYER_INVULNERABLE_TICKS,
                            new LifeState(numberOfLives - 1,
                                    State.INVULNERABLE))
                    .concat(Sq.constant(new LifeState(numberOfLives - 1,
                            State.VULNERABLE)));
        }
    }

    /**
     * Getter for the current number of lives of the player.
     * 
     * @return the current number of lives of the player
     */
    public int lives() {

        return lifeState().lives();
    }

    /**
     * Checks if the player is alive.
     * 
     * @return true if the players number of lives is greater than zero
     */
    public boolean isAlive() {

        return lives() > 0;
    }

    /**
     * Getter for the sequence of directed positions of the player.
     * 
     * @return the sequence of directed positions of the player
     */
    public Sq<DirectedPosition> directedPositions() {

        return directedPos;
    }

    /**
     * Getter for the current position of the player.
     * 
     * @return the position of the head of the sequence of directed positions of
     *         the player
     */
    public SubCell position() {

        return directedPositions().head().position();
    }

    /**
     * Getter for the current direction of the player.
     * 
     * @return the direction of the head of the sequence of directed positions
     *         of the player
     */
    public Direction direction() {

        return directedPositions().head().direction();
    }

    /**
     * Getter for the max number of bombs the player can place simultaneously.
     * 
     * @return the number of maximum bombs the player can place simultaneously
     */
    public int maxBombs() {

        return maxBombs;
    }

    /**
     * Creates a identical player, the only difference is the new number of
     * maximum bombs.
     * 
     * @param newMaxBombs
     *            the new number of maximum bombs
     * @return the new identical player but with the new maximum number of bombs
     */
    public Player withMaxBombs(int newMaxBombs) {

        return new Player(id(), lifeStates(), directedPositions(), newMaxBombs,
                bombRange());
    }

    /**
     * Getter for the range of the explosion of the players bombs.
     * 
     * @return the range of the explosion of the players bombs
     */
    public int bombRange() {

        return bombRange;
    }

    /**
     * Creates a identical player, the only difference is the new range of the
     * bombs.
     * 
     * @param newBombRange
     *            the new range of the bombs
     * @return the new identical player but with the new explosion range of the
     *         bombs
     */
    public Player withBombRange(int newBombRange) {

        return new Player(id(), lifeStates(), directedPositions(), maxBombs(),
                newBombRange);
    }

    /**
     * Places a bomb on the containing cell of the player.
     * 
     * @return a new bomb placed on the containing cell of the players sub-cell
     */
    public Bomb newBomb() {

        return new Bomb(id(), position().containingCell(),
                Ticks.BOMB_FUSE_TICKS, bombRange());
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------------------------------------

    public static final class LifeState {

        /**
         * This class emulates a pair composed of a number of lives and a state.
         * 
         * @author Maire Cedric (259314)
         * @author Délèze Benjamin (259992)
         *
         */

        /*
         * The two constants that qualify the pair life-state; the number of
         * lives and its current state.
         */
        private final int lives;
        private final State state;

        /**
         * Constructor of the LifeState class that initializes the two
         * attributes (number of lives and the state).
         * 
         * @param lives
         *            integer representing the number of lives at the creation
         * @param state
         *            the state of the player at the creation
         * @throws IllegalArgumentException
         *             if the parameter lives is strictly negative
         * @throws NullPointerException
         *             if the parameter states is null
         */
        public LifeState(int lives, State state) {
            this.lives = ArgumentChecker.requireNonNegative(lives);
            this.state = Objects.requireNonNull(state);
        }

        /**
         * Getter for the attribute that represents the number of lives.
         * 
         * @return the number of lives of the corresponding player
         */
        public int lives() {

            return lives;
        }

        /**
         * Getter for the current state of life.
         * 
         * @return the current state of life of the corresponding player
         */
        public State state() {

            return state;
        }

        /**
         * Method to know if the player can move or not.
         * 
         * @return true if the state is INVULNERABLE or VULNERABLE, false
         *         otherwise
         */
        public boolean canMove() {

            return state() == State.INVULNERABLE || state() == State.VULNERABLE;
        }

        // -----------------------------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------------------------

        public enum State {

            /**
             * Enumeration representing the different possible sates of a
             * player.
             * 
             * @author Maire Cedric (259314)
             * @author Délèze Benjamin (259992)
             *
             */

            /*
             * INVULNERABLE = the player is invulnerable against explosions and
             * does not lose life points; VULNERABLE = its normal state, the
             * player is vulnerable against explosions and can lose life points;
             * DYING = when the player is dying; DEAD = when the player has no
             * life points left and is dead.
             */
            INVULNERABLE, VULNERABLE, DYING, DEAD;
        }
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------------------------------------

    public static final class DirectedPosition {

        /**
         * This class emulates a pair composed of a direction and a position.
         * 
         * @author Maire Cedric (259314)
         * @author Délèze Benjamin (259992)
         *
         */

        /*
         * The two constants that qualify the pair direction-position; its
         * current position and its direction.
         */
        private final SubCell position;
        private final Direction direction;

        /**
         * Constructor of the DirectedPosition class that initializes the two
         * attributes (its position and direction).
         * 
         * @param position
         *            the position to give
         * @param direction
         *            the direction to give
         * @throws NullPointerException
         *             if one of the parameter is null
         */
        public DirectedPosition(SubCell position, Direction direction) {
            this.position = Objects.requireNonNull(position);
            this.direction = Objects.requireNonNull(direction);
        }

        /**
         * Method that will represent a stopped player.
         * 
         * @param p
         *            the directed position that will represent the stopped
         *            player
         * @return an infinite sequence only composed of the directed position
         *         given in the parameter
         */
        public static Sq<DirectedPosition> stopped(DirectedPosition p) {

            return Sq.constant(p);
        }

        /**
         * Method that will represent a moving player.
         * 
         * @param p
         *            the directed position that qualify the moving player
         * @return an infinite sequence composed of the directed position given
         *         in the parameter as the first element and all the neighbors
         *         in the direction given by the parameter
         */
        public static Sq<DirectedPosition> moving(DirectedPosition p) {

            return Sq.iterate(p,
                    u -> new DirectedPosition(
                            u.position().neighbor(u.direction()),
                            u.direction()));
        }

        /**
         * Getter for the position attribute.
         * 
         * @return the position
         */
        public SubCell position() {

            return position;
        }

        /**
         * Method to create a new DirectedPosition with the new position given
         * as parameter but with the same direction as before.
         * 
         * @param newPosition
         *            the new position to give
         * @return the new DirectedPosition with the new position and the same
         *         direction as before
         */
        public DirectedPosition withPosition(SubCell newPosition) {

            return new DirectedPosition(newPosition, direction());
        }

        /**
         * Getter for the direction.
         * 
         * @return the direction
         */
        public Direction direction() {

            return direction;
        }

        /**
         * Method to create a new DirectedPosition with the new direction given
         * as parameter but with the same position as before.
         * 
         * @param newDirection
         *            the new direction to give
         * @return the new DirectedPosition with the new direction but the same
         *         position as before
         */
        public DirectedPosition withDirection(Direction newDirection) {

            return new DirectedPosition(position(), newDirection);
        }
    }
}