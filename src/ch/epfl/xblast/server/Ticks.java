package ch.epfl.xblast.server;

import ch.epfl.xblast.Time;

/**
 * This interface defines the number of ticks for different aspects and action
 * in the game.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public interface Ticks {

    // Period during which the player is dying.
    public static final int PLAYER_DYING_TICKS = 8;

    // Period during which the player is invincible.
    public static final int PLAYER_INVULNERABLE_TICKS = 64;

    // Ticks before the bombs explodes.
    public static final int BOMB_FUSE_TICKS = 100;

    // Duration of the explosion.
    public static final int EXPLOSION_TICKS = 30;

    // Duration of the walls collapsing. (Same as EXPLOSION_TICKS)
    public static final int WALL_CRUMBLING_TICKS = EXPLOSION_TICKS;

    /*
     * Number of ticks to make a bonus disappear after being hit by a bomb.
     * (Same as EXPLOSION_TICKS)
     */
    public static final int BONUS_DISAPPEARING_TICKS = EXPLOSION_TICKS;

    // Number of ticks contained in one second.
    public static final int TICKS_PER_SECOND = 20;

    // The duration of a tick in nanoseconds.
    public static final int TICK_NANOSECOND_DURATION = Time.NS_PER_S
            / TICKS_PER_SECOND;

    // Total number of ticks for one game.
    public static final int TOTAL_TICKS = Time.TOTAL_MINUTES * Time.S_PER_MIN
            * TICKS_PER_SECOND;
}