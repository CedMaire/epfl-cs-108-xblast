package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Lists;

/**
 * This class represents a board of the game XBlast2016.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Board {

    // List containing all the sequences of each block (in row major order).
    private final List<Sq<Block>> blockSequences;

    /**
     * The constructor of the board, initializing the sequences of the blocks.
     * 
     * @param blocks
     *            list of sequences of the blocks
     */
    public Board(List<Sq<Block>> blocks) {
        if (Objects.requireNonNull(blocks).size() != Cell.COUNT) {
            throw new IllegalArgumentException();
        }

        blockSequences = Collections
                .unmodifiableList(new ArrayList<Sq<Block>>(blocks));
    }

    /**
     * Creates a constant board with the block matrix given.
     * 
     * @param rows
     *            block matrix
     * @return a constant board created with the parameter
     */
    public static Board ofRows(List<List<Block>> rows) {
        checkBlockMatrix(rows, Cell.ROWS, Cell.COLUMNS);

        List<Sq<Block>> temporaryList = new ArrayList<>();
        for (List<Block> listOfBlocks : rows) {
            for (Block block : listOfBlocks) {
                temporaryList.add(Sq.constant(block));
            }
        }

        return new Board(temporaryList);
    }

    /**
     * Creates a walled board with the inner blocks given.
     * 
     * @param innerBlocks
     *            the inner blocks given
     * @return a walled board with the inner blocks from the parameter
     */
    public static Board ofInnerBlocksWalled(List<List<Block>> innerBlocks) {
        checkBlockMatrix(innerBlocks, Cell.ROWS - 2, Cell.COLUMNS - 2);

        List<List<Block>> temporaryList = new ArrayList<>();
        copyUnmutableListOfListOfObject(innerBlocks, temporaryList);

        for (List<Block> listOfBlocks : temporaryList) {
            listOfBlocks.add(0, Block.INDESTRUCTIBLE_WALL);
            listOfBlocks.add(Block.INDESTRUCTIBLE_WALL);
        }

        temporaryList.add(0,
                Collections.nCopies(Cell.COLUMNS, Block.INDESTRUCTIBLE_WALL));
        temporaryList.add(
                Collections.nCopies(Cell.COLUMNS, Block.INDESTRUCTIBLE_WALL));

        return ofRows(temporaryList);
    }

    /**
     * Creates a symmetric and walled board with one North-West quadrant block.
     * 
     * @param quadrantNWBlocks
     *            the North-West quadrant block
     * @return a symmetric and walled board
     */
    public static Board ofQuadrantNWBlocksWalled(
            List<List<Block>> quadrantNWBlocks) {
        checkBlockMatrix(quadrantNWBlocks, Cell.ROWS / 2, Cell.COLUMNS / 2);

        List<List<Block>> temporaryList = new ArrayList<>();
        copyUnmutableListOfListOfObject(quadrantNWBlocks, temporaryList);

        temporaryList = new ArrayList<>(Lists.mirrored(temporaryList));

        for (int i = 0; i < temporaryList.size(); i++) {
            temporaryList.set(i, Lists.mirrored(temporaryList.get(i)));
        }

        return ofInnerBlocksWalled(temporaryList);
    }

    /**
     * Returns the sequence of the block types of the cell given.
     * 
     * @param c
     *            the cell we want the sequence of
     * @return the sequence of the given cell
     */
    public Sq<Block> blocksAt(Cell c) {

        return blockSequences.get(c.rowMajorIndex());
    }

    /**
     * Return the block type of the cell given.
     * 
     * @param c
     *            the cell we want the block type of
     * @return the block type for the given cell
     */
    public Block blockAt(Cell c) {

        return blocksAt(c).head();
    }

    /**
     * Checks if the matrix has the right dimensions.
     * 
     * @param matrix
     *            two dimensional list containing the blocks
     * @param rows
     *            right number of rows
     * @param columns
     *            right number of columns
     * @throws IllegalArgumentException
     *             if the block matrix does not have the right dimensions
     */
    private static void checkBlockMatrix(List<List<Block>> matrix, int rows,
            int columns) {
        if (matrix.size() != rows) {
            throw new IllegalArgumentException();
        }

        for (List<Block> listOfBlocks : matrix) {
            if (listOfBlocks.size() != columns) {
                throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Method to make a deep copy of an immutable list so we can work on it.
     * 
     * @param unmutableList
     *            the immutable list of lists we want a copy of
     * @param copyList
     *            the empty list that will be a copy of the immutable list
     */
    private static <T> void copyUnmutableListOfListOfObject(
            List<List<T>> unmutableList, List<List<T>> copyList) {
        for (List<T> listOfObjectT : unmutableList) {
            copyList.add(new ArrayList<T>(listOfObjectT));
        }
    }
}