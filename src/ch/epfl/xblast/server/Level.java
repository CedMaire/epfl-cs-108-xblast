package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.graphics.BlockImage;
import ch.epfl.xblast.server.graphics.BoardPainter;

/**
 * This class represents a level of the game.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Level {

    // Some constants for the default players.
    private static final Cell[] DEFAULT_POSITIONS = { new Cell(1, 1),
            new Cell(13, 1), new Cell(13, 11), new Cell(1, 11) };
    private static final int DEFAULT_LIVES = 3;
    private static final int DEFAULT_BOMBS = 2;
    private static final int DEFAULT_RANGE = 3;

    // Constant to get the default level of the game.
    public static final Level DEFAULT_LEVEL = new Level(defaultBoardPainter(),
            defaultGameState());

    // Two attributes that represent a level, a board painter and a game state.
    private final BoardPainter boardPainter;
    private final GameState gameState;

    /**
     * Constructor for the Level class.
     * 
     * @param boardPainter
     *            the painter for the board
     * @param gameState
     *            the initial game state
     * @throws NullPointerException
     *             if one of the parameters is null
     */
    public Level(BoardPainter boardPainter, GameState gameState) {
        this.boardPainter = Objects.requireNonNull(boardPainter);
        this.gameState = Objects.requireNonNull(gameState);
    }

    /**
     * Getter for the attribute boardPainter.
     * 
     * @return the board painter
     */
    public BoardPainter boardPainter() {

        return boardPainter;
    }

    /**
     * Getter for the attribute gameState.
     * 
     * @return the game state
     */
    public GameState gameState() {

        return gameState;
    }

    /**
     * Private method to construct the default board painter for the
     * DEFAULT_LEVEL.
     * 
     * @return the default board painter
     */
    private static BoardPainter defaultBoardPainter() {
        Map<Block, BlockImage> blockToBlockImage = new HashMap<Block, BlockImage>();

        for (int i = 0; i < Block.values().length; i++) {
            if (i < 1) {
                blockToBlockImage.put(Block.values()[i],
                        BlockImage.values()[i]);
            } else {
                blockToBlockImage.put(Block.values()[i],
                        BlockImage.values()[i + 1]);
            }
        }

        return new BoardPainter(blockToBlockImage, BlockImage.IRON_FLOOR_S);
    }

    /**
     * Private method to construct the default game state for the DEFAULT_LEVEL.
     * 
     * @return the default game state
     */
    private static GameState defaultGameState() {
        Block __ = Block.FREE;
        Block XX = Block.INDESTRUCTIBLE_WALL;
        Block xx = Block.DESTRUCTIBLE_WALL;

        List<List<Block>> board = Arrays.asList(
                Arrays.asList(__, __, __, __, __, xx, __),
                Arrays.asList(__, XX, xx, XX, xx, XX, xx),
                Arrays.asList(__, xx, __, __, __, xx, __),
                Arrays.asList(xx, XX, __, XX, XX, XX, XX),
                Arrays.asList(__, xx, __, xx, __, __, __),
                Arrays.asList(xx, XX, xx, XX, xx, XX, __));

        List<Player> players = new ArrayList<>();
        for (int i = 0; i < PlayerID.values().length; i++) {
            players.add(new Player(PlayerID.values()[i], DEFAULT_LIVES,
                    DEFAULT_POSITIONS[i], DEFAULT_BOMBS, DEFAULT_RANGE));
        }

        return new GameState(Board.ofQuadrantNWBlocksWalled(board), players);
    }
}