package ch.epfl.xblast.server.graphics;

/**
 * This enumeration defines the different images of the blocks.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum BlockImage {

    // Enumeration for all the different images of blocks.
    IRON_FLOOR, IRON_FLOOR_S, DARK_BLOCK, EXTRA, EXTRA_O, BONUS_BOMB, BONUS_RANGE;
}