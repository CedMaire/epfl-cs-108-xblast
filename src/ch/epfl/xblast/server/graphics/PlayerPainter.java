package ch.epfl.xblast.server.graphics;

import ch.epfl.xblast.server.Player;
import ch.epfl.xblast.server.Player.LifeState.State;

/**
 * This class represents a painter for the players.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class PlayerPainter {

    // Byte used to identify the image to use if the player is dead.
    private static final byte BYTE_FOR_DEAD = 14;

    /*
     * Two constants to identify the base shift for the ID and the direction of
     * the player and two constants to identify the byte for the two last images
     * (losing life and dying). And one last constant if the player is
     * invulnerable and the tick is odd.
     */
    private static final byte BASE_ID_SHIFT = 20;
    private static final byte BASE_DIRECTION_SHIFT = 3;
    private static final byte BYTE_FOR_LOSING_LIFE = 12;
    private static final byte BYTE_FOR_DYING = 13;
    private static final byte ID_SHIFT_INVUL_ODDTICK = 80;

    /*
     * In each direction the player has three different images that can be used,
     * here we define the constants for the different moves. This shift is
     * computed by taking the rest of the division by 4 of the x or y
     * coordinate.
     */
    private static final byte FIRST_STEP_SHIFT = 0;
    private static final byte SECOND_STEP_SHIFT = 1;
    private static final byte THIRD_STEP_SHIFT = 2;
    private static final int COORD_MODULO = 4;

    /**
     * Non instanciable class.
     */
    private PlayerPainter() {

    }

    /**
     * Given a tick and a player, this method return the byte identifying the
     * image to use for the given player.
     * 
     * @param tick
     *            the current tick of the game
     * @param player
     *            the player we want the image of
     * @return the byte that identifies the image to use for the given player
     */
    public static byte byteForPlayer(int tick, Player player) {
        // If the player is dead we just return the constant BYTE_FOR_DEAD.
        if (!player.isAlive()) {

            return BYTE_FOR_DEAD;
        }

        // A variable to store the shift corresponding to the players ID.
        byte idAddition = (byte) (BASE_ID_SHIFT * player.id().ordinal());

        // If the player is dying we return one of the two last images.
        if (player.lifeState().state() == State.DYING) {

            return (byte) (player.lives() > 1
                    ? BYTE_FOR_LOSING_LIFE + idAddition
                    : BYTE_FOR_DYING + idAddition);
        }

        /*
         * Two variables to store the shift corresponding to the players
         * direction and position.
         */
        byte directionAddition = (byte) (BASE_DIRECTION_SHIFT
                * player.direction().ordinal());
        byte positionAddition = player.direction().isHorizontal()
                ? getPositionAddition(player.position().x())
                : getPositionAddition(player.position().y());

        /*
         * If the player is invulnerable and the tick is odd, we use the
         * corresponding white image.
         */
        if (player.lifeState().state() == State.INVULNERABLE && tick % 2 != 0) {
            idAddition = ID_SHIFT_INVUL_ODDTICK;
        }

        return (byte) (idAddition + directionAddition + positionAddition);
    }

    /**
     * Computes the modulo of the coordinate with 4 and returns the shift for
     * the image.
     * 
     * @param coord
     *            coordinate x or y (depending on the direction of the player)
     * @return the shift for the image to use
     */
    private static byte getPositionAddition(int coord) {
        if (coord % COORD_MODULO == 1) {

            return SECOND_STEP_SHIFT;
        } else if (coord % COORD_MODULO == 3) {

            return THIRD_STEP_SHIFT;
        } else {

            return FIRST_STEP_SHIFT;
        }
    }
}