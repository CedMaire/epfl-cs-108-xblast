package ch.epfl.xblast.server.graphics;

import ch.epfl.xblast.Direction;
import ch.epfl.xblast.server.Bomb;

/**
 * This class represents a painter for the bombs and explosions.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class ExplosionPainter {

    // Byte used to identify the image to use if there is no blast on the cell.
    public static final byte BYTE_FOR_EMPTY = 16;

    /*
     * Two constants to identify the two images for the bomb and the bomb in
     * white.
     */
    private static final byte BOMB = 20;
    private static final byte BOMB_WHITE = 21;

    // Three constants for the blasts.
    private static final byte BINARY_BASE = 2;
    private static final byte MAX_BLAST_BYTE = 0b1000;
    private static final byte NO_BLAST_BYTE = 0b0000;

    /**
     * Non instanciable class.
     */
    private ExplosionPainter() {

    }

    /**
     * Given a bomb, this method returns the byte identifying the image to use
     * for the given bomb.
     * 
     * @param bomb
     *            the bomb we want the image of
     * @return the byte that identifies the image to use for the given bomb
     */
    public static byte byteForBomb(Bomb bomb) {

        return (byte) (Integer.bitCount(bomb.fuseLength()) == 1 ? BOMB_WHITE
                : BOMB);
    }

    /**
     * Given four boolean (that state if a blast is on the neighbor cell), this
     * method returns the byte identifying the image to use for the blast.
     * 
     * @param northBlast
     *            true if there is a blast on the neighbor to the north
     * @param eastBlast
     *            true if there is a blast on the neighbor to the east
     * @param southBlast
     *            true if there is a blast on the neighbor to the south
     * @param westBlast
     *            true if there is a blast on the neighbor to the west
     * @return the byte that identifies the image to use for the blast
     */
    public static byte byteForBlast(boolean northBlast, boolean eastBlast,
            boolean southBlast, boolean westBlast) {
        byte northByte = (byte) (northBlast
                ? MAX_BLAST_BYTE / Math.pow(BINARY_BASE, Direction.N.ordinal())
                : NO_BLAST_BYTE);
        byte eastByte = (byte) (eastBlast
                ? MAX_BLAST_BYTE / Math.pow(BINARY_BASE, Direction.E.ordinal())
                : NO_BLAST_BYTE);
        byte southByte = (byte) (southBlast
                ? MAX_BLAST_BYTE / Math.pow(BINARY_BASE, Direction.S.ordinal())
                : NO_BLAST_BYTE);
        byte westByte = (byte) (westBlast
                ? MAX_BLAST_BYTE / Math.pow(BINARY_BASE, Direction.W.ordinal())
                : NO_BLAST_BYTE);

        return (byte) (northByte + eastByte + southByte + westByte);
    }
}