package ch.epfl.xblast.server.graphics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.server.Block;
import ch.epfl.xblast.server.Board;

/**
 * This class represents a painter for the board.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class BoardPainter {

    /*
     * Two attributes needed by the board painter. A map that maps each type of
     * Block to the corresponding image and the image to use if the free Block
     * has a shadow on it.
     */
    private final Map<Block, BlockImage> blockToBlockImage;
    private final BlockImage shadowedBlock;

    /**
     * Constructor for the BoardPainter class.
     * 
     * @param blockToBlockImage
     *            map each type of Block to the corresponding image
     * @param shadowedBlock
     *            image to use if the free Block has a shadow on it
     * @throws NullPointerException
     *             if one of the parameters is null
     */
    public BoardPainter(Map<Block, BlockImage> blockToBlockImage,
            BlockImage shadowedBlock) {
        this.blockToBlockImage = Collections.unmodifiableMap(
                new HashMap<>(Objects.requireNonNull(blockToBlockImage)));
        this.shadowedBlock = Objects.requireNonNull(shadowedBlock);
    }

    /**
     * Given a board and a cell, this method returns the byte identifying the
     * image to use for the given cell.
     * 
     * @param board
     *            the current board of the game
     * @param cell
     *            the cell we want the image of
     * @return the byte that identifies the image to use for the given cell
     */
    public byte byteForCell(Board board, Cell cell) {
        if (board.blockAt(cell).isFree()
                && board.blockAt(cell.neighbor(Direction.W)).castsShadow()) {

            return (byte) shadowedBlock.ordinal();
        } else {

            return (byte) blockToBlockImage.get(board.blockAt(cell)).ordinal();
        }
    }
}