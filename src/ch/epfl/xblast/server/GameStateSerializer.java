package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.RunLengthEncoder;
import ch.epfl.xblast.Time;
import ch.epfl.xblast.server.graphics.BoardPainter;
import ch.epfl.xblast.server.graphics.ExplosionPainter;
import ch.epfl.xblast.server.graphics.PlayerPainter;

/**
 * This class represents a game state serializer.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class GameStateSerializer {

    /**
     * Non instanciable class.
     */
    private GameStateSerializer() {

    }

    /**
     * Given a board painter and a game state, this method returns the
     * serialized version of the state.
     * 
     * @param boardPainter
     *            the board painter of the current game
     * @param gameState
     *            the current game state
     * @return the serialized version of the current sate
     */
    public static List<Byte> serialize(BoardPainter boardPainter,
            GameState gameState) {
        List<Byte> stateSerial = new ArrayList<>();

        stateSerial
                .addAll(gameBoardSerializer(gameState.board(), boardPainter));
        stateSerial.addAll(bombExplosionSerializer(gameState));
        stateSerial.addAll(playersSerializer(gameState));
        stateSerial.add(remainingTimeSerializer(gameState.remainingTime()));

        return Collections.unmodifiableList(stateSerial);
    }

    /**
     * Given a board and a board painter, this method return the serialized
     * version of the game board.
     * 
     * @param board
     *            the board of the game
     * @param boardPainter
     *            the board painter of the game
     * @return the serialized version of the game board
     */
    private static List<Byte> gameBoardSerializer(Board board,
            BoardPainter boardPainter) {
        List<Byte> gameBoardSerial = new ArrayList<>();

        for (Cell cell : Cell.SPIRAL_ORDER) {
            gameBoardSerial.add(boardPainter.byteForCell(board, cell));
        }

        gameBoardSerial = new ArrayList<>(
                RunLengthEncoder.encode(gameBoardSerial));
        gameBoardSerial.add(0, (byte) gameBoardSerial.size());

        return gameBoardSerial;
    }

    /**
     * Given a gameState, this method returns the serialized version of the
     * bombs and blasts in the game.
     * 
     * @param gameState
     *            the current game state
     * @return the serialized version of the bombs and explosions
     */
    private static List<Byte> bombExplosionSerializer(GameState gameState) {
        List<Byte> bombExplosionSerial = new ArrayList<>();

        for (Cell cell : Cell.ROW_MAJOR_ORDER) {
            /*
             * If the current block is not free or the block contains no blast
             * and no bomb, then we add the convention byte that says there is
             * nothing.
             */
            if (!gameState.board().blockAt(cell).isFree()
                    || (!gameState.bombedCells().containsKey(cell)
                            && !gameState.blastedCells().contains(cell))) {
                bombExplosionSerial.add(ExplosionPainter.BYTE_FOR_EMPTY);
            }
            // Here the block contains a bomb.
            else if (gameState.bombedCells().containsKey(cell)) {
                bombExplosionSerial.add(ExplosionPainter
                        .byteForBomb(gameState.bombedCells().get(cell)));
            }
            // Here the block contains a blast.
            else {
                boolean northBlast = gameState.blastedCells()
                        .contains(cell.neighbor(Direction.N));
                boolean eastBlast = gameState.blastedCells()
                        .contains(cell.neighbor(Direction.E));
                boolean southBlast = gameState.blastedCells()
                        .contains(cell.neighbor(Direction.S));
                boolean westBlast = gameState.blastedCells()
                        .contains(cell.neighbor(Direction.W));

                bombExplosionSerial.add(ExplosionPainter.byteForBlast(
                        northBlast, eastBlast, southBlast, westBlast));
            }
        }

        bombExplosionSerial = new ArrayList<>(
                RunLengthEncoder.encode(bombExplosionSerial));
        bombExplosionSerial.add(0, (byte) (bombExplosionSerial.size()));

        return bombExplosionSerial;
    }

    /**
     * Given a game state, this method return the serialized version of the
     * state of the players.
     * 
     * @param gameState
     *            the current game state
     * @return the serialized version of the players
     */
    private static List<Byte> playersSerializer(GameState gameState) {
        List<Byte> playersSerial = new ArrayList<>();

        for (Player player : gameState.players()) {
            playersSerial.add((byte) player.lives());
            playersSerial.add((byte) player.position().x());
            playersSerial.add((byte) player.position().y());
            playersSerial.add(
                    PlayerPainter.byteForPlayer(gameState.ticks(), player));
        }

        return playersSerial;
    }

    /**
     * Given the remaining time of the game, this method return the serialized
     * version of the time remaining.
     * 
     * @param remainingTime
     *            the remaining time of the game
     * @return the serialized version of the remaining time of the game
     */
    private static Byte remainingTimeSerializer(double remainingTime) {

        return (byte) Math.ceil((remainingTime / Time.TOTAL_MINUTES));
    }
}