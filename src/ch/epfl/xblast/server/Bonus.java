package ch.epfl.xblast.server;

/**
 * This enumeration represents the different bonuses a player can have active.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum Bonus {

    /*
     * The possible bonuses, one to increase the maximum number of bombs a
     * player can place simultaneously in the game and one to increase the range
     * of those bombs.
     */
    INC_BOMB {
        @Override
        /**
         * Applies the INC_BOMB bonus to the player.
         * 
         * @return a new player with the new maximum bombs number
         */
        public Player applyTo(Player player) {

            return player
                    .withMaxBombs(Math.min(MAX_BOMBS, player.maxBombs() + 1));
        }
    },
    INC_RANGE {
        @Override
        /**
         * Applies the INC_RANGE bonus to the player.
         * 
         * @return a new player with the new maximum bomb range
         */
        public Player applyTo(Player player) {

            return player
                    .withBombRange(Math.min(MAX_RANGE, player.bombRange() + 1));
        }
    };

    /*
     * Constants representing the maximum number of bombs a player can place
     * simultaneously and the max range of these bombs.
     */
    private static final int MAX_BOMBS = 9;
    private static final int MAX_RANGE = 9;

    abstract public Player applyTo(Player player);
}