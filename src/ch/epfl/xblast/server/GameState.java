package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.Lists;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.SubCell;
import ch.epfl.xblast.server.Player.DirectedPosition;
import ch.epfl.xblast.server.Player.LifeState;
import ch.epfl.xblast.server.Player.LifeState.State;

/**
 * This class represents the state of a game (server-side).
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class GameState {

    /*
     * These are all the attributes characterizing the state of a game: the
     * ticks number when the game starts, the game board, the list of players,
     * the list of bombs, the list of explosions and the list of blasts
     */
    private final int ticks;
    private final Board board;
    private final List<Player> players;
    private final List<Bomb> bombs;
    private final List<Sq<Sq<Cell>>> explosions;
    private final List<Sq<Cell>> blasts;

    // All permutations of all the player IDs.
    private static final List<List<PlayerID>> playerPermutations = Lists
            .permutations(Arrays.asList(PlayerID.values()));

    // Instance to generate pseudo-random numbers.
    private static final Random RANDOM = new Random(2016);

    // The minimal distance allowed to the next bomb we are heading to.
    private static final int MIN_DISTANCE_TO_BOMB_ALLOWED = 6;

    /*
     * Fixed array for the three possibilities when a crumbling wall collapses.
     */
    private static final Block[] BLOCK_CHOICES = { Block.BONUS_BOMB,
            Block.BONUS_RANGE, Block.FREE };

    /**
     * Constructor that initializes the game with all the given informations.
     * 
     * @param ticks
     *            the ticks of the game start
     * @param board
     *            the board for the game
     * @param players
     *            all the players
     * @param bombs
     *            all the bombs
     * @param explosions
     *            the explosions
     * @param blasts
     *            the blasts
     * @throws IllegalArgumentException
     *             if the ticks number is strictly negative or the number of
     *             players is not equal to 4
     * @throws NullPointerException
     *             if one of the given objects (board, players, bombs,
     *             explosions, blasts) is null
     */
    public GameState(int ticks, Board board, List<Player> players,
            List<Bomb> bombs, List<Sq<Sq<Cell>>> explosions,
            List<Sq<Cell>> blasts) {
        this.ticks = ArgumentChecker.requireNonNegative(ticks);
        this.board = Objects.requireNonNull(board);

        this.players = Collections.unmodifiableList(
                new ArrayList<>(Objects.requireNonNull(players)));
        if (this.players.size() != PlayerID.values().length) {
            throw new IllegalArgumentException();
        }

        this.bombs = Collections.unmodifiableList(
                new ArrayList<>(Objects.requireNonNull(bombs)));
        this.explosions = Collections.unmodifiableList(
                new ArrayList<>(Objects.requireNonNull(explosions)));
        this.blasts = Collections.unmodifiableList(
                new ArrayList<>(Objects.requireNonNull(blasts)));
    }

    /**
     * This constructor helps constructing the game state for the beginning, the
     * start of the game.
     * 
     * @param board
     *            the game board
     * @param players
     *            the list of players
     */
    public GameState(Board board, List<Player> players) {

        this(0, board, players, new ArrayList<Bomb>(),
                new ArrayList<Sq<Sq<Cell>>>(), new ArrayList<Sq<Cell>>());
    }

    /**
     * Returns the tick corresponding to the current state.
     * 
     * @return the tick corresponding to the current state
     */
    public int ticks() {

        return ticks;
    }

    /**
     * Decides if the game is over or not.
     * 
     * @return true if the current tick equals or is over Ticks.TOTAL_TICKS or
     *         if there is one or less player left, false otherwise
     */
    public boolean isGameOver() {

        return remainingTime() < 0 || alivePlayers().size() <= 1;
    }

    /**
     * Computes and return the game time left (in seconds).
     * 
     * @return the game time left (in seconds)
     */
    public double remainingTime() {

        return (double) (Ticks.TOTAL_TICKS - ticks()) / Ticks.TICKS_PER_SECOND;
    }

    /**
     * Returns the identity of the winner (if there is one).
     * 
     * @return the identity of the winner (if there is one), an empty optional
     *         otherwise
     */
    public Optional<PlayerID> winner() {
        Optional<PlayerID> optionalWinner;

        if (alivePlayers().size() == 1) {
            optionalWinner = Optional.of(alivePlayers().get(0).id());
        } else {
            optionalWinner = Optional.empty();
        }

        return optionalWinner;
    }

    /**
     * Returns the board of the game.
     * 
     * @return the game board
     */
    public Board board() {

        return board;
    }

    /**
     * Returns all the players in the game (dead or alive).
     * 
     * @return all the players in the game (dead or alive)
     */
    public List<Player> players() {

        return players;
    }

    /**
     * Returns a list containing only the alive players.
     * 
     * @return a list of all alive players
     */
    public List<Player> alivePlayers() {
        List<Player> alivePlayers = new ArrayList<>();

        for (Player player : players()) {
            if (player.isAlive()) {
                alivePlayers.add(player);
            }
        }

        return Collections.unmodifiableList(alivePlayers);
    }

    /**
     * Method that returns a map which associate each bomb to its cell.
     * 
     * @return a map associating each bomb to its cell
     */
    public Map<Cell, Bomb> bombedCells() {
        Map<Cell, Bomb> temporaryBombedCells = new HashMap<>();

        for (Bomb bomb : bombs) {
            temporaryBombedCells.put(bomb.position(), bomb);
        }

        return Collections.unmodifiableMap(temporaryBombedCells);
    }

    /**
     * Method that returns a set of cells that contain at least one blast.
     * 
     * @return a set of cells that contain at least one blast
     */
    public Set<Cell> blastedCells() {
        Set<Cell> temporaryBlastedCells = new HashSet<>();

        for (Sq<Cell> sqOfCell : blasts) {
            temporaryBlastedCells.add(sqOfCell.head());
        }

        return Collections.unmodifiableSet(temporaryBlastedCells);
    }

    /**
     * Return the game state for the next tick according to the current game
     * state and the given events.
     * 
     * @param speedChangeEvents
     *            map mapping the player IDs with their desired next direction
     * @param bombDropEvents
     *            the players who want to drop a bomb
     * @return the game state for the next tick
     */
    public GameState next(Map<PlayerID, Optional<Direction>> speedChangeEvents,
            Set<PlayerID> bombDropEvents) {
        // Map mapping each player ID to its player.
        Map<PlayerID, Player> temporaryMapPlayerIdToPlayer = new HashMap<>();
        /*
         * Stocks the current permutation of the players (needed to resolve all
         * conflicts).
         */
        List<Player> currentPlayerPermutation = new ArrayList<>();
        // Set containing all the consumed bonuses.
        Set<Cell> temporaryConsumedBonuses = new HashSet<>();
        // Map mapping each player ID to its consumed bonus.
        Map<PlayerID, Bonus> temporaryMapPlayerIdToBonuses = new HashMap<>();

        // Create the map temporaryMapPlayerIdToPlayer.
        for (Player player : players()) {
            temporaryMapPlayerIdToPlayer.put(player.id(), player);
        }

        /*
         * Create the list currentPlayerPermutation in the right order (the
         * order corresponding to the current tick).
         */
        for (PlayerID playerID : playerPermutations
                .get(ticks() % playerPermutations.size())) {
            currentPlayerPermutation
                    .add(temporaryMapPlayerIdToPlayer.get(playerID));
        }

        /*
         * Initialization of the two variables temporaryMapPlayerIdToBonuses and
         * temporaryConsumedBonuses.
         */
        for (Player player : currentPlayerPermutation) {
            if (board().blockAt(player.position().containingCell()).isBonus()
                    && player.position().isCentral()) {

                // Mapping of the player to its consumed bonus.
                if (!(temporaryConsumedBonuses
                        .contains(player.position().containingCell()))) {
                    temporaryMapPlayerIdToBonuses.put(player.id(),
                            board().blockAt(player.position().containingCell())
                                    .associatedBonus());
                }

                // Adds the consumed bonuses to the set.
                temporaryConsumedBonuses
                        .add(player.position().containingCell());
            }
        }

        // List of the blasts for the next tick.
        List<Sq<Cell>> temporaryNextBlasts = nextBlasts(blasts, board(),
                explosions);
        /*
         * Set containing all the cell that are blasted (with respect to
         * temporaryNextBlasts).
         */
        Set<Cell> temporarySetOfBlastedCells = new HashSet<>();

        // Initialization of the set of the next blasted cells.
        for (Sq<Cell> sqOfBlasts : temporaryNextBlasts) {
            temporarySetOfBlastedCells.add(sqOfBlasts.head());
        }

        // List of the explosions for the next tick.
        List<Sq<Sq<Cell>>> temporaryExplosions = nextExplosions(explosions);

        // List containing the newly dropped bombs and the current bombs.
        List<Bomb> temporaryBombs = newlyDroppedBombs(currentPlayerPermutation,
                bombDropEvents, bombs);
        temporaryBombs.addAll(bombs);
        // Lists of the bombs in temporaryBombs that have not exploded.
        List<Bomb> temporaryBombsForNextTick = new ArrayList<>();

        /*
         * Consumes the fuse of the bombs or they explode if the fuse length is
         * zero or touched by a blast.
         */
        for (Bomb bomb : temporaryBombs) {
            if (temporarySetOfBlastedCells.contains(bomb.position())
                    || bomb.fuseLengths().tail().isEmpty()) {
                temporaryExplosions.addAll(bomb.explosion());
            } else {
                temporaryBombsForNextTick.add(new Bomb(bomb.ownerId(),
                        bomb.position(), bomb.fuseLength() - 1, bomb.range()));
            }
        }

        // Set containing the cells that contain a bomb.
        Set<Cell> temporarySetOfBombedCells = new HashSet<>();

        // Creates the set of bombed cells.
        for (Bomb bomb : temporaryBombsForNextTick) {
            temporarySetOfBombedCells.add(bomb.position());
        }

        // Board for the next tick.
        Board temporaryBoard = nextBoard(board(), temporaryConsumedBonuses,
                temporarySetOfBlastedCells);
        // List of players for the next tick.
        List<Player> temporaryPlayers = nextPlayers(players(),
                temporaryMapPlayerIdToBonuses, temporarySetOfBombedCells,
                temporaryBoard, temporarySetOfBlastedCells, speedChangeEvents);

        return new GameState(ticks() + 1, temporaryBoard, temporaryPlayers,
                temporaryBombsForNextTick, temporaryExplosions,
                temporaryNextBlasts);
    }

    /**
     * Computes the explosions blasts fort the next game state.
     * 
     * @param blasts0
     *            the list of all current blast particles
     * @param board0
     *            the current board
     * @param explosions0
     *            the list of the new explosions
     * @return a list of all the blasts for the next game state
     */
    private static List<Sq<Cell>> nextBlasts(List<Sq<Cell>> blasts0,
            Board board0, List<Sq<Sq<Cell>>> explosions0) {
        List<Sq<Cell>> temporaryNextBlasts = new ArrayList<>();

        for (Sq<Cell> currentBlast : blasts0) {
            if (board0.blockAt(currentBlast.head()).isFree()
                    && !currentBlast.tail().isEmpty()) {
                temporaryNextBlasts.add(currentBlast.tail());
            }
        }

        for (Sq<Sq<Cell>> newExplosion : explosions0) {
            temporaryNextBlasts.add(newExplosion.head());
        }

        return temporaryNextBlasts;
    }

    /**
     * Computes the next state of the game board relative to the the current
     * board, the consumed bonuses and the new explosion blasts.
     * 
     * @param board0
     *            the current board
     * @param consumedBonuses
     *            the consumed bonuses
     * @param blastedCells1
     *            the new explosion blasts
     * @return the new board according to the new game states
     */
    private static Board nextBoard(Board board0, Set<Cell> consumedBonuses,
            Set<Cell> blastedCells1) {
        /*
         * List of all the sequences of the blocks that will represent the
         * board.
         */
        List<Sq<Block>> temporaryBlockSequences = new ArrayList<>();

        // For over all the cells.
        for (Cell cell : Cell.ROW_MAJOR_ORDER) {
            // Block of the current cell and block sequence of the current cell.
            Block currentBlock = board0.blockAt(cell);
            Sq<Block> currentSqOfCurrentBlock = board0.blocksAt(cell);

            /*
             * If the set of consumed bonuses contains the current cell we
             * replace it with a free block.
             */
            if (consumedBonuses.contains(cell)) {
                temporaryBlockSequences.add(Sq.constant(Block.FREE));
            }
            // If the set of blasted cells contains the current cell.
            else if (blastedCells1.contains(cell)) {
                // And the currentBlock is a bonus, he will be destroyed.
                if (currentBlock.isBonus()) {
                    temporaryBlockSequences.add(currentSqOfCurrentBlock
                            .limit(Ticks.BONUS_DISAPPEARING_TICKS + 1)
                            .concat(Sq.constant(Block.FREE)).tail());
                }
                // And if the block is a destructible wall he will be destroyed.
                else if (currentBlock == Block.DESTRUCTIBLE_WALL) {
                    int randomInteger = RANDOM.nextInt(3);

                    temporaryBlockSequences.add(Sq
                            .repeat(Ticks.WALL_CRUMBLING_TICKS,
                                    Block.CRUMBLING_WALL)
                            .concat(Sq.constant(BLOCK_CHOICES[randomInteger])));
                }
                // Otherwise the block just continues its life.
                else {
                    temporaryBlockSequences.add(currentSqOfCurrentBlock.tail());
                }
            }
            // Otherwise the block just continues its life.
            else {
                temporaryBlockSequences.add(currentSqOfCurrentBlock.tail());
            }
        }

        return new Board(temporaryBlockSequences);
    }

    /**
     * Computes the next state for each player with respect to the given
     * parameters.
     * 
     * @param players0
     *            the list of the current players
     * @param playerBonuses
     *            map mapping the player IDs with their consumed bonus
     * @param bombedCells1
     *            all the bombed cells for the next game state
     * @param board1
     *            the board for the next board
     * @param blastedCells1
     *            all the blasted cell for the next game state
     * @param speedChangeEvents
     *            map mapping the player IDs with their desired next direction
     * @return the list of all the next players
     */
    private static List<Player> nextPlayers(List<Player> players0,
            Map<PlayerID, Bonus> playerBonuses, Set<Cell> bombedCells1,
            Board board1, Set<Cell> blastedCells1,
            Map<PlayerID, Optional<Direction>> speedChangeEvents) {
        List<Player> temporaryNextPlayers = new ArrayList<>();

        for (Player player : players0) {
            Sq<DirectedPosition> temporaryNextDirectedPosition;

            /*
             * If the player wants to change it's direction or to stop (not
             * move).
             */
            if (speedChangeEvents.containsKey(player.id())) {
                if (speedChangeEvents.get(player.id()).isPresent()) {
                    /*
                     * If the player wants to go in the same or opposite
                     * direction. He can do it immediately.
                     */
                    if (player.direction().isParallelTo(
                            speedChangeEvents.get(player.id()).get())) {
                        temporaryNextDirectedPosition = DirectedPosition
                                .moving(player.directedPositions().head()
                                        .withDirection(speedChangeEvents
                                                .get(player.id()).get()));
                    }
                    /*
                     * If the player wants to turn. Here he has to get to the
                     * central sub-cell first.
                     */
                    else {
                        SubCell findFirstCentralSubCell = player
                                .directedPositions()
                                .findFirst(u -> u.position().isCentral())
                                .position();
                        DirectedPosition findFirstCentralSubCellNewDirection = new DirectedPosition(
                                findFirstCentralSubCell,
                                speedChangeEvents.get(player.id()).get());

                        temporaryNextDirectedPosition = DirectedPosition
                                .moving(player.directedPositions().head())
                                .takeWhile(u -> !(u.position().isCentral()))
                                .concat(DirectedPosition.moving(
                                        findFirstCentralSubCellNewDirection));
                    }
                }
                /*
                 * If the player is in the map, but contains no mapping for the
                 * key. The player continues it's way in the same direction as
                 * before until the first central sub-cell and then stops.
                 */
                else {
                    SubCell findFirstCentralSubCell = player.directedPositions()
                            .findFirst(u -> u.position().isCentral())
                            .position();
                    DirectedPosition findFirstCentralSubCellNewDirection = new DirectedPosition(
                            findFirstCentralSubCell, player.direction());

                    temporaryNextDirectedPosition = DirectedPosition
                            .moving(player.directedPositions().head())
                            .takeWhile(u -> !(u.position().isCentral()))
                            .concat(DirectedPosition.stopped(
                                    findFirstCentralSubCellNewDirection));
                }
            }
            /*
             * If the player is not in the map he has to continues his way in
             * the same direction as before.
             */
            else {
                temporaryNextDirectedPosition = player.directedPositions();
            }

            // Here we check if the player is allowed to move.
            if (player.lifeState().canMove()) {
                Direction directionToConsider;

                // The direction to consider is the one given in the map.
                if (speedChangeEvents.get(player.id()) != null
                        && speedChangeEvents.get(player.id()).isPresent()) {
                    directionToConsider = speedChangeEvents.get(player.id())
                            .get();
                }
                // If there is no given direction, take the current direction.
                else {
                    directionToConsider = player.direction();
                }

                /*
                 * Searching for the first sub-cell contained in another cell
                 * (than player).
                 */
                if (!player.position().isCentral() || board1
                        .blockAt(player.position().containingCell()
                                .neighbor(directionToConsider))
                        .canHostPlayer()) {
                    // Check if the player will be blocked by a bomb.
                    if (!(bombedCells1
                            .contains(player.position().containingCell())
                            && player.position()
                                    .distanceToCentral() == MIN_DISTANCE_TO_BOMB_ALLOWED
                            && temporaryNextDirectedPosition.tail().head()
                                    .position()
                                    .distanceToCentral() == MIN_DISTANCE_TO_BOMB_ALLOWED
                                            - 1)) {
                        temporaryNextDirectedPosition = temporaryNextDirectedPosition
                                .tail();
                    }
                }
            }

            Sq<LifeState> temporaryNextLifeState;

            // If the player is touched by the blast of a bomb.
            if (blastedCells1
                    .contains(temporaryNextDirectedPosition.head().position()
                            .containingCell())
                    && player.lifeState().state() == State.VULNERABLE) {
                temporaryNextLifeState = player.statesForNextLife();
            }
            // If the player is not touched by the blast of a bomb.
            else {
                temporaryNextLifeState = player.lifeStates().tail();
            }

            // Creation of the player for the next game state.
            Player temporaryNextPlayer = new Player(player.id(),
                    temporaryNextLifeState, temporaryNextDirectedPosition,
                    player.maxBombs(), player.bombRange());

            // If the player has taken a bonus.
            if (playerBonuses.get(player.id()) != null) {
                temporaryNextPlayer = playerBonuses.get(player.id())
                        .applyTo(temporaryNextPlayer);
            }

            temporaryNextPlayers.add(temporaryNextPlayer);
        }

        return temporaryNextPlayers;
    }

    /**
     * Computes the next explosions for the next state according the current
     * explosions.
     * 
     * @param explosions0
     *            the current explosions
     * @return a list of explosions
     */
    private static List<Sq<Sq<Cell>>> nextExplosions(
            List<Sq<Sq<Cell>>> explosions0) {
        List<Sq<Sq<Cell>>> temporaryNextExplosions = new ArrayList<>();

        for (Sq<Sq<Cell>> sqOfSqOfCell : explosions0) {
            if (!(sqOfSqOfCell.tail().isEmpty())) {
                temporaryNextExplosions.add(sqOfSqOfCell.tail());
            }
        }

        return temporaryNextExplosions;
    }

    /**
     * Returns the newly dropped bombs.
     * 
     * @param players0
     *            all the players in the game
     * @param bombDropEvents
     *            the IDs of the players who want to drop a bomb
     * @param bombs0
     *            the current bombs in the game
     * @return a list of newly dropped bombs
     */
    private static List<Bomb> newlyDroppedBombs(List<Player> players0,
            Set<PlayerID> bombDropEvents, List<Bomb> bombs0) {
        // Lists containing all the newly dropped bombs.
        List<Bomb> temporaryNewlyDroppedBombs = new ArrayList<>();
        // Map mapping each player to its number of bombs placed in the game.
        Map<PlayerID, Integer> playerIdMapToNumberBombs = new HashMap<>();
        // Set containing all the cells that contain a bomb.
        Set<Cell> cellContainingBomb = new HashSet<>();

        // Creating the map playerIdMapToNumberBombs.
        for (PlayerID playerID : PlayerID.values()) {
            playerIdMapToNumberBombs.put(playerID, 0);
        }

        /*
         * Updating the map playerIdMapToNumberBombs and filling the set
         * cellContainingBomb.
         */
        for (Bomb bomb : bombs0) {
            playerIdMapToNumberBombs.replace(bomb.ownerId(),
                    playerIdMapToNumberBombs.get(bomb.ownerId()) + 1);
            cellContainingBomb.add(bomb.position());
        }

        /*
         * If the player wants and is allowed to drop a bomb, we add it in
         * temporaryNewlyDroppedBombs and update the set cellContainingBomb.
         */
        for (Player player : players0) {
            if (bombDropEvents.contains(player.id()) && player.isAlive()
                    && !(cellContainingBomb
                            .contains(player.position().containingCell()))
                    && playerIdMapToNumberBombs.get(player.id()) < player
                            .maxBombs()) {
                temporaryNewlyDroppedBombs.add(new Bomb(player.id(),
                        player.position().containingCell(),
                        Ticks.BOMB_FUSE_TICKS, player.bombRange()));
                cellContainingBomb.add(player.position().containingCell());
            }
        }

        return temporaryNewlyDroppedBombs;
    }
}