package ch.epfl.xblast.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ch.epfl.cs108.Sq;
import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Cell;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerID;

/**
 * This class emulates a bomb.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Bomb {

    /*
     * Constants that qualify a bomb (its owner ID, its position, its time to
     * detonate and its range).
     */
    private final PlayerID ownerId;
    private final Cell position;
    private final Sq<Integer> fuseLengths;
    private final int range;

    /**
     * Constructor of the Bomb class.
     * 
     * @param ownerId
     *            the owner of the bomb
     * @param position
     *            the position of the bomb
     * @param fuseLengths
     *            the sequence representing the time needed to detonate
     * @param range
     *            the range in which the bomb explodes
     * @throws NullPointerException
     *             if ownerId, position or fuseLengths are null
     * @throws IllegalArgumentException
     *             if fuseLengths is empty or the range is strictly negative
     */
    public Bomb(PlayerID ownerId, Cell position, Sq<Integer> fuseLengths,
            int range) {
        this.ownerId = Objects.requireNonNull(ownerId);
        this.position = Objects.requireNonNull(position);
        this.fuseLengths = Objects.requireNonNull(fuseLengths);

        if (fuseLengths.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.range = ArgumentChecker.requireNonNegative(range);
    }

    /**
     * Constructor of the Bomb class.
     * 
     * @param ownerId
     *            the owner of the bomb
     * @param position
     *            the position of the bomb
     * @param fuseLength
     *            the time needed to detonate
     * @param range
     */
    public Bomb(PlayerID ownerId, Cell position, int fuseLength, int range) {

        this(ownerId, position,
                Sq.iterate(fuseLength, u -> u - 1).limit(fuseLength), range);
    }

    /**
     * Returns the owner ID of the Bomb.
     * 
     * @return the ID of the owner of the Bomb
     */
    public PlayerID ownerId() {

        return ownerId;
    }

    /**
     * Returns the position of the Bomb.
     * 
     * @return the position of the Bomb
     */
    public Cell position() {

        return position;
    }

    /**
     * Returns the sequence representing the time needed to detonate.
     * 
     * @return the sequence representing the time needed to detonate
     */
    public Sq<Integer> fuseLengths() {

        return fuseLengths;
    }

    /**
     * Returns the length of the current fuse.
     * 
     * @return the length of the current fuse
     */
    public int fuseLength() {

        return fuseLengths().head();
    }

    /**
     * Returns the range of the Bomb.
     * 
     * @return the range of the Bomb
     */
    public int range() {

        return range;
    }

    /**
     * Returns the explosion of the corresponding Bomb represented by an array
     * of 4 elements, each represents an arm.
     * 
     * @return a list of 4 elements, each representing an arm
     */
    public List<Sq<Sq<Cell>>> explosion() {
        List<Sq<Sq<Cell>>> temporaryArmsList = new ArrayList<>();

        for (Direction direction : Direction.values()) {
            temporaryArmsList.add(explosionArmTowards(direction));
        }

        return Collections.unmodifiableList(temporaryArmsList);
    }

    /**
     * Computes the arm of explosion directed in the direction given by the
     * parameter.
     * 
     * @param dir
     *            the direction we want the arm explodes
     * @return the exploding arm in the direction given by the parameter
     */
    private Sq<Sq<Cell>> explosionArmTowards(Direction dir) {

        return Sq.repeat(Ticks.EXPLOSION_TICKS,
                Sq.iterate(position(), u -> u.neighbor(dir)).limit(range()));
    }
}