package ch.epfl.xblast.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import ch.epfl.xblast.ArgumentChecker;
import ch.epfl.xblast.Direction;
import ch.epfl.xblast.PlayerAction;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.Time;
import ch.epfl.xblast.server.graphics.BoardPainter;

/**
 * This class is the main class of the server for the game XBlast2016.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Main {

    /*
     * This is the default number of clients we use it if the number of clients
     * is not specified. And the default port to use.
     */
    private static final int DEFAULT_NBR_CLIENTS = PlayerID.values().length;
    private static final int DEFAULT_PORT = 2016;

    /*
     * Each PlayerAction is encoded in only one byte. And the player ID we
     * receive is also encoded in only one byte.
     */
    private static final int PLAYER_ACTION_BYTE = 1;
    private static final int PLAYER_ID_BYTE = 1;

    // We need a place holder for the ID we transmit to the clients.
    private static final byte PLACE_HOLDER = -1;

    // Buffers used to receive and send the informations.
    private static ByteBuffer bufferSender;
    private static final ByteBuffer bufferReceiverID = ByteBuffer
            .allocate(PLAYER_ID_BYTE);
    private static final ByteBuffer bufferReceiverAction = ByteBuffer
            .allocate(PLAYER_ACTION_BYTE);

    /*
     * A map to store the addresses of the players and map them to their player
     * ID.
     */
    private static final Map<SocketAddress, PlayerID> mapIDToAdress = new HashMap<>();

    /*
     * A map to store the change of direction for the players and a set of the
     * players who want to drop a bomb.
     */
    private static final Map<PlayerID, Optional<Direction>> speedChangeEvents = new EnumMap<>(
            PlayerID.class);
    private static final Set<PlayerID> bombDropEvents = EnumSet
            .noneOf(PlayerID.class);

    // A little variable to store the address of the packets we receive.
    private static SocketAddress senderAddress;

    /*
     * Four variables to store the game state, the board painter, the number of
     * players wanted for the game and the serial of the game state.
     */
    private static GameState gameState = Level.DEFAULT_LEVEL.gameState();
    private static BoardPainter boardPainter = Level.DEFAULT_LEVEL
            .boardPainter();
    private static int clientsNumber = DEFAULT_NBR_CLIENTS;
    private static List<Byte> serial;

    /**
     * Main method that is executed on the server.
     * 
     * @param args
     *            optionally contains the number of players wanted for the game
     */
    public static void main(String[] args) throws Exception {
        /*
         * We initialize the number of clients we want to have or leave the
         * default number.
         */
        if (args.length > 0
                && ArgumentChecker
                        .requireNonNegative(Integer.parseInt(args[0])) != 0
                && Integer.parseInt(args[0]) < DEFAULT_NBR_CLIENTS) {
            clientsNumber = Integer.parseInt(args[0]);
        }

        try (DatagramChannel channel = DatagramChannel
                .open(StandardProtocolFamily.INET)) {
            channel.bind(new InetSocketAddress(DEFAULT_PORT));

            /*
             * We wait until we have the right number of clients connected.
             * (Phase 1)
             */
            waitForClientsToConnect(channel);

            // We now enter phase 2 and stay here until the game is over.
            channel.configureBlocking(false);

            final long START_TIME = System.nanoTime();
            while (!gameState.isGameOver()) {
                computeSerialAndSend(channel);
                sleepIfWeCan(START_TIME);
                gatherPlayersActions(channel);

                gameState = gameState.next(speedChangeEvents, bombDropEvents);
            }

            /*
             * The last thing is to display the winner, if there is one. (Phase
             * 3)
             */
            if (gameState.winner().isPresent()) {
                System.out.println("Aaaaand the WINNEEEEEER iiiiis "
                        + gameState.winner().get()
                        + "! (clap clap clap clap...)");
            } else {
                System.out.println("There is no WINNER! You're all n00bs...");
            }
        }
    }

    /**
     * Waits for the right number of clients to connect.
     * 
     * @param channel
     *            the communication channel used
     */
    private static void waitForClientsToConnect(DatagramChannel channel)
            throws IOException {
        while (mapIDToAdress.size() < clientsNumber) {
            bufferReceiverID.clear();
            senderAddress = channel.receive(bufferReceiverID);
            bufferReceiverID.rewind();

            if (bufferReceiverID.hasRemaining() && bufferReceiverID
                    .get() == PlayerAction.JOIN_GAME.ordinal()) {
                mapIDToAdress.putIfAbsent(senderAddress,
                        PlayerID.values()[mapIDToAdress.size()]);
            }
        }
    }

    /**
     * Method to compute the serial and send it to the clients.
     * 
     * @param channel
     *            the communication channel used
     */
    private static void computeSerialAndSend(DatagramChannel channel)
            throws IOException {
        serial = GameStateSerializer.serialize(boardPainter, gameState);

        /*
         * We allocate the place needed for the serial and the player ID and we
         * put a place holder for the player ID.
         */
        bufferSender = ByteBuffer.allocate(serial.size() + PLAYER_ID_BYTE);
        bufferSender.put(PLACE_HOLDER);

        // We put the serial into the buffer.
        for (Byte currentByte : serial) {
            bufferSender.put(currentByte);
        }

        // We send the game state to each client.
        for (Map.Entry<SocketAddress, PlayerID> currentEntry : mapIDToAdress
                .entrySet()) {
            bufferSender.put(0, (byte) currentEntry.getValue().ordinal());
            bufferSender.rewind();

            channel.send(bufferSender, currentEntry.getKey());
        }
    }

    /**
     * We test if we have some time left to sleep.
     * 
     * @param startTime
     *            the start time of the game
     */
    private static void sleepIfWeCan(long startTime)
            throws InterruptedException {
        long timeForTickN = startTime + (long) (gameState.ticks() + 1)
                * Ticks.TICK_NANOSECOND_DURATION;
        long timeToSleep = timeForTickN - System.nanoTime();

        if (timeToSleep > 0) {
            Thread.sleep(timeToSleep / Time.US_PER_S,
                    (int) (timeToSleep % Time.US_PER_S));
        }
    }

    /**
     * Method to gather the players actions.
     * 
     * @param channel
     *            the communication channel used
     */
    private static void gatherPlayersActions(DatagramChannel channel)
            throws IOException {
        speedChangeEvents.clear();
        bombDropEvents.clear();

        bufferReceiverAction.clear();
        senderAddress = channel.receive(bufferReceiverAction);

        while (senderAddress != null) {
            bufferReceiverAction.rewind();

            switch (PlayerAction.values()[bufferReceiverAction.get()]) {
            case MOVE_N:
                speedChangeEvents.put(mapIDToAdress.get(senderAddress),
                        Optional.of(Direction.N));
                break;
            case MOVE_E:
                speedChangeEvents.put(mapIDToAdress.get(senderAddress),
                        Optional.of(Direction.E));
                break;
            case MOVE_S:
                speedChangeEvents.put(mapIDToAdress.get(senderAddress),
                        Optional.of(Direction.S));
                break;
            case MOVE_W:
                speedChangeEvents.put(mapIDToAdress.get(senderAddress),
                        Optional.of(Direction.W));
                break;
            case STOP:
                speedChangeEvents.put(mapIDToAdress.get(senderAddress),
                        Optional.empty());
                break;
            case DROP_BOMB:
                bombDropEvents.add(mapIDToAdress.get(senderAddress));
                break;
            default:
                // We do nothing.
                break;
            }

            bufferReceiverAction.clear();
            senderAddress = channel.receive(bufferReceiverAction);
        }
    }
}