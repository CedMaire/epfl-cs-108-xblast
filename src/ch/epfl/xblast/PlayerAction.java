package ch.epfl.xblast;

/**
 * This enumeration shows all the different actions a human player can perform.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum PlayerAction {

    /*
     * Some actions a player can perform (in this order) to join the game, to
     * move to the north, the the east, to the south and to the west. One to
     * stop and a last one to drop a bomb.
     */
    JOIN_GAME, MOVE_N, MOVE_E, MOVE_S, MOVE_W, STOP, DROP_BOMB;
}