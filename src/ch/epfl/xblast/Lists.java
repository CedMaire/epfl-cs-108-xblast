package ch.epfl.xblast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This class contains static methods to work on lists.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Lists {

    /**
     * Non instanciable class.
     */
    private Lists() {

    }

    /**
     * Method to create a symmetric version of the list given.
     * 
     * @param l
     *            the list we want a symmetric version of
     * @return the symmetric version of the parameter l
     * @throws IllegalArgumentException
     *             if the parameter l is empty
     */
    public static <T> List<T> mirrored(List<T> l) {
        if (l.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            List<T> temporaryList = new ArrayList<>(l.subList(0, l.size() - 1));
            List<T> lCopy = new ArrayList<>(l);

            Collections.reverse(temporaryList);
            lCopy.addAll(temporaryList);

            return Collections.unmodifiableList(lCopy);
        }
    }

    /**
     * A static method to compute and return all permutations of a list of
     * objects. Method inspired by the code from this source:
     * http://stackoverflow.com/questions/10503392/java-code-for-permutations-of
     * -a-list-of-numbers
     * 
     * @param l
     *            the list of objects we want all the permutations of
     * @return a list of lists containing all the permutations of the list given
     *         in parameter
     */
    public static <T> List<List<T>> permutations(List<T> l) {
        List<List<T>> temporaryPermutationList = new ArrayList<>();

        /*
         * If the list given as parameter is empty or has only one element we
         * can return it without changing anything.
         */
        if (Objects.requireNonNull(l).isEmpty() || l.size() == 1) {
            temporaryPermutationList
                    .add(Collections.unmodifiableList(new ArrayList<T>(l)));
        }
        /*
         * If the list has more than one element, we need to compute the
         * permutations.
         */
        else {
            List<T> lCopy = new ArrayList<>(l);
            T head = lCopy.remove(0);

            /*
             * Recursive computation of the permutations of the tail of the
             * list.
             */
            for (List<T> allPermutations : permutations(lCopy)) {
                List<List<T>> allSubLists = new ArrayList<>();

                /*
                 * Now we put the head of the list in each possible position in
                 * the tail.
                 */
                for (int i = 0; i <= allPermutations.size(); i++) {
                    List<T> currentSubList = new ArrayList<>();

                    currentSubList.addAll(allPermutations);
                    currentSubList.add(i, head);
                    allSubLists
                            .add(Collections.unmodifiableList(currentSubList));
                }

                temporaryPermutationList
                        .addAll(Collections.unmodifiableList(allSubLists));
            }
        }

        return Collections.unmodifiableList(temporaryPermutationList);
    }
}