package ch.epfl.xblast;

/**
 * Enumeration representing the four players.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public enum PlayerID {

    // Each player will have an attributed player ID, that is unique.
    PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4;
}