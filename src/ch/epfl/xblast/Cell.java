package ch.epfl.xblast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class emulates a Cell.
 * 
 * @author Maire Cedric (259314)
 * @author Délèze Benjamin (259992)
 *
 */

public final class Cell {

    /*
     * Three static constants to stock the number of columns, rows and total
     * cells.
     */
    public static final int COLUMNS = 15;
    public static final int ROWS = 13;
    public static final int COUNT = COLUMNS * ROWS;

    /*
     * These two lists stocks the cells in a particular order (row major order
     * and spiral order).
     */
    public static final List<Cell> ROW_MAJOR_ORDER = Collections
            .unmodifiableList(computeRowMajorOrder());
    public static final List<Cell> SPIRAL_ORDER = Collections
            .unmodifiableList(computeSpiralOrder());

    // Two constants to stock the x and y coordinates of each cell.
    private final int coordX;
    private final int coordY;

    /**
     * The constructor of the Cell class, it initializes the x and y
     * coordinates.
     * 
     * @param x
     *            the x coordinate
     * @param y
     *            the y coordinate
     */
    public Cell(int x, int y) {
        coordX = Math.floorMod(x, COLUMNS);
        coordY = Math.floorMod(y, ROWS);
    }

    /**
     * Getter for the x coordinate.
     * 
     * @return the x coordinate
     */
    public int x() {

        return coordX;
    }

    /**
     * Getter for the y coordinate.
     * 
     * @return the y coordinate
     */
    public int y() {

        return coordY;
    }

    /**
     * Method to get the index (in row major order) of the cell.
     * 
     * @return the index of the cell
     */
    public int rowMajorIndex() {

        return y() * COLUMNS + x();
    }

    /**
     * Method to get the neighbor of the cell in the desired direction.
     * 
     * @param dir
     *            the desired direction
     * @return the neighbor cell
     */
    public Cell neighbor(Direction dir) {
        switch (dir) {
        case N:
            return new Cell(x(), y() - 1);

        case E:
            return new Cell(x() + 1, y());

        case S:
            return new Cell(x(), y() + 1);

        case W:
            return new Cell(x() - 1, y());
        // We just throw an Error because it is supposed to never happen.
        default:
            throw new Error();
        }
    }

    @Override
    /**
     * Redefinition of the equals() method of the Object class.
     * 
     * @param that
     *            the cell to be compared with
     * @return true if they have the same class, x and y coordinate; false
     *         otherwise
     */
    public boolean equals(Object that) {

        return that.getClass() == this.getClass()
                && ((Cell) that).x() == this.x()
                && ((Cell) that).y() == this.y();
    }

    @Override
    /**
     * Redefinition of the toString() method of the Object class.
     * 
     * @return a string showing the x and y coordinate of the cell (Format:
     *         (xCoordinate, yCoordinate))
     */
    public String toString() {

        return "(" + x() + ", " + y() + ")";
    }

    /**
     * Computing the dynamic table of the cells (in row major order).
     * 
     * @return an ArrayList containing the cells in row major order
     */
    private static ArrayList<Cell> computeRowMajorOrder() {
        ArrayList<Cell> temporaryList = new ArrayList<>();

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                temporaryList.add(new Cell(j, i));
            }
        }

        return temporaryList;
    }

    /**
     * Computing the dynamic table of the cells (in spiral order). We coded the
     * pseudo-code given in the .pdf in Java.
     * 
     * @return an ArrayList containing the cells in spiral order
     */
    private static ArrayList<Cell> computeSpiralOrder() {
        ArrayList<Cell> temporaryList = new ArrayList<>();

        ArrayList<Integer> iX = new ArrayList<>();
        for (int i = 0; i < COLUMNS; i++) {
            iX.add(i);
        }

        ArrayList<Integer> iY = new ArrayList<>();
        for (int i = 0; i < ROWS; i++) {
            iY.add(i);
        }

        boolean isHorizontal = true;

        while (!(iX.isEmpty()) && !(iY.isEmpty())) {
            ArrayList<Integer> i1;
            ArrayList<Integer> i2;

            if (isHorizontal) {
                i1 = iX;
                i2 = iY;
            } else {
                i1 = iY;
                i2 = iX;
            }

            int c2 = i2.get(0);
            i2.remove(0);

            for (int i : i1) {
                Cell temporaryCell;

                if (isHorizontal) {
                    temporaryCell = new Cell(i, c2);
                } else {
                    temporaryCell = new Cell(c2, i);
                }

                temporaryList.add(temporaryCell);
            }

            Collections.reverse(i1);
            isHorizontal = !isHorizontal;
        }

        return temporaryList;
    }

    @Override
    /**
     * Redefinition of the hashCode() method of the Object class.
     * 
     * @return a hash code value for this object
     */
    public int hashCode() {

        return rowMajorIndex();
    }
}