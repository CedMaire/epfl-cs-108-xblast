package ch.epfl.xblast.server.debug;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.epfl.xblast.Cell;
import ch.epfl.xblast.PlayerID;
import ch.epfl.xblast.server.Block;
import ch.epfl.xblast.server.Board;
import ch.epfl.xblast.server.Bomb;
import ch.epfl.xblast.server.GameState;
import ch.epfl.xblast.server.Player;

public final class GameStatePrinterAntonio {

    private GameStatePrinterAntonio() {

    }

    public static void printGameState(GameState s) {
        List<Player> ps = s.alivePlayers();
        Board board = s.board();
        Map<Cell, Bomb> bombs = s.bombedCells();
        Set<Cell> blasts = s.blastedCells();
        for (int y = 0; y < Cell.ROWS; ++y) {
            xLoop: for (int x = 0; x < Cell.COLUMNS; ++x) {
                Cell c = new Cell(x, y);
                for (Player p : ps) {
                    if (p.position().containingCell().equals(c)) {
                        System.out.print(stringForPlayer(p));
                        continue xLoop;
                    }
                }
                if (blasts.contains(c))
                    System.out.print(stringForBlast());
                else if (bombs.get(c) != null) {
                    System.out.print(stringForBomb());
                } else {
                    Block b = board.blockAt(c);
                    System.out.print(stringForBlock(b));
                }
            }
            System.out.println();
        }
    }

    private static String stringForPlayer(Player p) {
        StringBuilder b = new StringBuilder();
        b.append(p.id().ordinal() + 1);
        switch (p.direction()) {
        case N:
            b.append('^');
            break;
        case E:
            b.append('>');
            break;
        case S:
            b.append('v');
            break;
        case W:
            b.append('<');
            break;
        }
        return b.toString();
    }

    private static String stringForBomb() {
        return "'O";
    }

    private static String stringForBlast() {
        return "**";
    }

    private static String stringForBlock(Block b) {
        switch (b) {
        case FREE:
            return "  ";
        case INDESTRUCTIBLE_WALL:
            return "##";
        case DESTRUCTIBLE_WALL:
            return "??";
        case CRUMBLING_WALL:
            return "¿¿";
        case BONUS_BOMB:
            return "+b";
        case BONUS_RANGE:
            return "+r";
        default:
            throw new Error();
        }
    }

    public static void main(String[] args) {
        List<List<Block>> temporaryList = new ArrayList<List<Block>>();
        temporaryList.add(Arrays.asList(Block.FREE, Block.FREE, Block.FREE,
                Block.FREE, Block.FREE, Block.DESTRUCTIBLE_WALL, Block.FREE));
        temporaryList.add(Arrays.asList(Block.FREE, Block.INDESTRUCTIBLE_WALL,
                Block.DESTRUCTIBLE_WALL, Block.INDESTRUCTIBLE_WALL,
                Block.DESTRUCTIBLE_WALL, Block.INDESTRUCTIBLE_WALL,
                Block.DESTRUCTIBLE_WALL));
        temporaryList.add(Arrays.asList(Block.FREE, Block.DESTRUCTIBLE_WALL,
                Block.FREE, Block.FREE, Block.FREE, Block.DESTRUCTIBLE_WALL,
                Block.FREE));
        temporaryList.add(Arrays.asList(Block.DESTRUCTIBLE_WALL,
                Block.INDESTRUCTIBLE_WALL, Block.FREE,
                Block.INDESTRUCTIBLE_WALL, Block.INDESTRUCTIBLE_WALL,
                Block.INDESTRUCTIBLE_WALL, Block.INDESTRUCTIBLE_WALL));
        temporaryList.add(Arrays.asList(Block.FREE, Block.DESTRUCTIBLE_WALL,
                Block.FREE, Block.DESTRUCTIBLE_WALL, Block.FREE, Block.FREE,
                Block.FREE));
        temporaryList.add(Arrays.asList(Block.DESTRUCTIBLE_WALL,
                Block.INDESTRUCTIBLE_WALL, Block.DESTRUCTIBLE_WALL,
                Block.INDESTRUCTIBLE_WALL, Block.DESTRUCTIBLE_WALL,
                Block.INDESTRUCTIBLE_WALL, Block.FREE));
        Board board = (Board.ofQuadrantNWBlocksWalled(temporaryList));
        List<Player> players = new ArrayList<>();
        players.add(new Player(PlayerID.PLAYER_1, 3, new Cell(1, 1), 3, 3));
        players.add(new Player(PlayerID.PLAYER_2, 3, new Cell(13, 1), 3, 3));
        players.add(new Player(PlayerID.PLAYER_3, 3, new Cell(1, 11), 3, 3));
        players.add(new Player(PlayerID.PLAYER_4, 3, new Cell(13, 11), 3, 3));
        GameState s = new GameState(board,
                Collections.unmodifiableList(players));
        printGameState(s);
        System.out.println();
        Set<PlayerID> bombDropEvents = new HashSet<>();
        bombDropEvents.add(PlayerID.PLAYER_1);
        s = s.next(null, bombDropEvents);
        players.set(0, new Player(PlayerID.PLAYER_1, 3, new Cell(1, 2), 3, 3));
        s = s.next(null, bombDropEvents);
        players.set(0, new Player(PlayerID.PLAYER_1, 3, new Cell(1, 3), 3, 3));
        s = s.next(null, bombDropEvents);
        players.set(0, new Player(PlayerID.PLAYER_1, 3, new Cell(1, 4), 3, 3));
        s = s.next(null, bombDropEvents);
        players.set(0, new Player(PlayerID.PLAYER_1, 3, new Cell(1, 5), 3, 3));
        s = s.next(null, bombDropEvents);
        players.set(0, new Player(PlayerID.PLAYER_1, 3, new Cell(1, 7), 3, 3));
        bombDropEvents.clear();
        printGameState(s);
        System.out.println();
        for (int i = 0; i < 96; ++i) {
            s = s.next(null, bombDropEvents);
        }
        printGameState(s);
        System.out.println();
        s = s.next(null, bombDropEvents);
        printGameState(s);
        System.out.println();
    }
}